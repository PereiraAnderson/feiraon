# FeiraON

An ASP.NET Core API REST Application and Angular 10 project.

This repo have the code already done, but the instructions below will guide you throw all the steps required to set up the application.

## Requirements

Before we start, you'll need the following packages:
- Docker
- Make*

## Preparing the environment
```
bash 

# With Make
make build && make run

# Without Make - See more commands in Makefile
docker build -f sqlserver.dockerfile -t feiraon/sqlserver:latest .
docker build -f back/Dockerfile -t feiraon/back:latest .
docker build -f front/Dockerfile -t feiraon/front:latest .
docker network create feiraon
docker volume create feiraon-sqlserver-volume
docker run --network feiraon --name feiraon-sqlserver -it -v feiraon-sqlserver-volume:/var/opt/mssql -p 1433:1433 feiraon/sqlserver:latest
docker run --network feiraon -e ASPNETCORE_ENVIRONMENT=Docker --name feiraon-back -d -p 5000:80 feiraon/back:latest
docker run --network feiraon --name feiraon-front -d -p 80:80 feiraon/front:latest

# Server will start at http://localhost:5000 and http://localhost
```

Try on http://35.225.110.67
