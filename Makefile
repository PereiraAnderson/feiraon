ENV:=Docker
MIGRATION:=Initial
SQLSERVER_VOLUME_NAME:=feiraon-sqlserver-volume

###################
# SQL SERVER
###################

stop-sqlserver:
	docker stop feiraon-sqlserver || exit 0
	docker rm -f feiraon-sqlserver || exit 0

build-sqlserver:
	docker build \
		-f sqlserver.dockerfile \
		-t feiraon/sqlserver:latest .

run-sqlserver: stop-sqlserver create-volume
	docker run --network feiraon \
		--name feiraon-sqlserver -d \
		-v $(SQLSERVER_VOLUME_NAME):/var/opt/mssql \
		-p 1433:1433 \
		feiraon/sqlserver:latest

run-sqlserver-it: stop-sqlserver create-volume
	docker run --network feiraon \
		--name feiraon-sqlserver -it \
		-v $(SQLSERVER_VOLUME_NAME):/var/opt/mssql \
		-p 1433:1433 \
		feiraon/sqlserver:latest

###################
# Back
###################

stop-back:
	docker stop feiraon-back || exit 0
	docker rm -f feiraon-back || exit 0

build-back:
	docker build \
		-f back/Dockerfile \
		-t feiraon/back:latest .

run-back: stop-back
	docker run --network feiraon \
		-e ASPNETCORE_ENVIRONMENT=$(ENV) \
		--name feiraon-back -d \
		-p 5000:80 \
		feiraon/back:latest

run-back-it: stop-back
	docker run --network feiraon \
		-e ASPNETCORE_ENVIRONMENT=$(ENV) \
		--name feiraon-back -it \
		-p 5000:80 \
		feiraon/back:latest

###################
# Front
###################

stop-front:
	docker stop feiraon-front || exit 0
	docker rm -f feiraon-front || exit 0

build-front:
	docker build \
		-f front/Dockerfile \
		-t feiraon/front:latest .

run-front: stop-front
	docker run --network feiraon \
		--name feiraon-front -d \
		-p 80:80 \
		feiraon/front:latest

run-front-it: stop-front
	docker run --network feiraon \
		--name feiraon-front -it \
		-p 80:80 \
		feiraon/front:latest

###################
# EXTRA
###################

create-network:
	docker network create feiraon || exit 0
	
create-volume:
	docker volume create $(SQLSERVER_VOLUME_NAME)

remove-volume:
	docker volume remove $(SQLSERVER_VOLUME_NAME)

build: create-network create-volume build-sqlserver build-back build-front
run: run-sqlserver run-back run-front
stop: stop-sqlserver stop-back stop-front
restart: stop build run

#LOGS
show-sqlserver: 
	docker logs -f feiraon-sqlserver
show-back: 
	docker logs -f feiraon-back
show-front: 
	docker logs -f feiraon-front

#DATABASE
create-migration:
	cd ./back && export ASPNETCORE_ENVIRONMENT=Migration && dotnet ef migrations add $(MIGRATION) -o ./Context/Migrations

revert-migration:
	cd ./back && export ASPNETCORE_ENVIRONMENT=Migration && dotnet ef database update $(MIGRATION)

remove-migration:
	cd ./back && export ASPNETCORE_ENVIRONMENT=Migration && dotnet ef migrations remove


#LOCAL
run-local: run-sqlserver run-local-back run-local-front

run-local-back:
	cd ./back &&  export ASPNETCORE_ENVIRONMENT=$(ENV) && \
	gnome-terminal -- bash -c "dotnet run  1>../back 2>../back.err"

run-local-front:
	cd ./front && npm start