import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgxWebstorageModule } from 'ngx-webstorage';
import { NgxMaskModule, IConfig } from 'ngx-mask';

import { AppMaterialModule } from './modules/app.material.module';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { ToolbarComponent } from './components/toolbar/toolbar.component';
import { LoginComponent } from './components/login/login.component';
import { UsuariosComponent } from './components/usuarios/usuarios.component';
import { AuthInterceptor } from './services/auth.interceptor';
import { HomeComponent } from './components/home/home.component';
import { UsuarioComponent } from './components/usuario/usuario.component';
import { CommonModule } from '@angular/common';
import { AuthGuardService } from './services/auth-guard.service';
import { EstabelecimentosComponent } from './components/estabelecimentos/estabelecimentos.component';
import { EstabelecimentoComponent } from './components/estabelecimento/estabelecimento.component';
import { ProdutosComponent } from './components/produtos/produtos.component';
import { ProdutoComponent } from './components/produto/produto.component';
import { PedidosComponent } from './components/pedidos/pedidos.component';
import { PedidoComponent } from './components/pedido/pedido.component';
import { EstoquesComponent } from './components/estoques/estoques.component';
import { EstoqueComponent } from './components/estoque/estoque.component';
import { CadastroUsuarioComponent } from './components/cadastro-usuario/cadastro-usuario.component';
import { CadastroEstabelecimentoComponent } from './components/cadastro-estabelecimento/cadastro-estabelecimento.component';
import { RelatorioVendaComponent } from './relatorios/relatorio-venda/relatorio-venda.component';
import { RelatorioAvaliacaoComponent } from './relatorios/relatorio-avaliacao/relatorio-avaliacao.component';
import { RelatorioEstoqueComponent } from './relatorios/relatorio-estoque/relatorio-estoque.component';

export const options: Partial<IConfig> | (() => Partial<IConfig>) = null;

@NgModule({
  declarations: [
    AppComponent,
    ToolbarComponent,
    LoginComponent,
    UsuariosComponent,
    HomeComponent,
    UsuarioComponent,
    EstabelecimentosComponent,
    EstabelecimentoComponent,
    ProdutosComponent,
    ProdutoComponent,
    PedidosComponent,
    PedidoComponent,
    EstoquesComponent,
    EstoqueComponent,
    CadastroUsuarioComponent,
    CadastroEstabelecimentoComponent,
    RelatorioVendaComponent,
    RelatorioAvaliacaoComponent,
    RelatorioEstoqueComponent
  ],
  imports: [
    CommonModule,
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    NgxWebstorageModule.forRoot(),
    NgxMaskModule.forRoot(),

    AppRoutingModule,
    AppMaterialModule
  ],
  providers: [
    AuthGuardService,
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
