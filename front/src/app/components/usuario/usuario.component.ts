import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { EstabelecimentoService } from 'src/app/api-services/estabelecimentoService';
import { UsuarioService } from 'src/app/api-services/usuarioService';
import { Estabelecimento } from 'src/app/models/estabelecimento';
import { Usuario } from 'src/app/models/usuario';
import { UtilService } from 'src/app/services/util.service';

@Component({
  selector: 'app-usuario',
  templateUrl: './usuario.component.html',
  styleUrls: ['./usuario.component.scss']
})
export class UsuarioComponent implements OnInit {
  usuario: Usuario;
  acao: string;
  perfis: string[];
  estabelecimentos: Estabelecimento[];

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private usuarioService: UsuarioService,
    private estabelecimentoService: EstabelecimentoService) {

    this.usuario = new Usuario();
    this.estabelecimentos = [];
  }

  ngOnInit() {
    this.perfis = UtilService.getValuesEnumUsuarioPerfil();

    this.estabelecimentoService.get()
      .then((data) => {
        this.estabelecimentos = data.listaItens.map(x => new Estabelecimento(x));
      })

    this.route.params.subscribe((params: Params) => {
      const id = params['id'];
      if (id != 0) {
        this.usuarioService.getById(id)
          .then((data) => {
            this.usuario = new Usuario(data)
          });
        this.acao = 'Editar';
      }
      else {
        this.acao = 'Criar';
      }
    });
  }

  onSubmit() {
    this.usuarioService.save(this.usuario)
      .then(() => this.router.navigate(['/usuarios']));
  }
}
