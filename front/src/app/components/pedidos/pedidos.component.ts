import { HttpParams } from '@angular/common/http';
import { Component } from '@angular/core';
import { PedidoService } from 'src/app/api-services/pedidoService';
import { EnumPedidoStatus } from 'src/app/models/enums/enumPedidoStatus';
import { EnumUsuarioPerfil } from 'src/app/models/enums/enumUsuarioPerfil';
import { Login } from 'src/app/models/login';
import { Pedido } from 'src/app/models/pedido';
import { SessionService } from 'src/app/services/session.service';
import { UtilService } from 'src/app/services/util.service';

@Component({
  selector: 'app-pedidos',
  templateUrl: './pedidos.component.html',
  styleUrls: ['./pedidos.component.scss']
})
export class PedidosComponent {
  displayedColumns: string[] = ['data', 'status', 'endereco', 'acoes'];
  data: Pedido[] = [];
  login: Login;

  isLoadingResults = true;
  isError = false;

  constructor(
    private pedidoService: PedidoService,
    private sessions: SessionService
  ) {
    this.login = this.sessions.getLogin();
    if (this.login.perfil === EnumUsuarioPerfil.Admin)
      this.displayedColumns = ['estabelecimento', 'cliente'].concat(this.displayedColumns);
    if (this.login.perfil === EnumUsuarioPerfil.Cliente)
      this.displayedColumns = ['estabelecimento'].concat(this.displayedColumns);
    if (this.login.perfil === EnumUsuarioPerfil.Estabelecimento)
      this.displayedColumns = ['cliente'].concat(this.displayedColumns);
  }

  ngAfterViewInit() {
    this.get();
  }

  get() {
    this.isLoadingResults = true;
    let params = new HttpParams()
      .set('Includes', 'Estabelecimento')
      .append('Includes', 'Usuario');

    if (this.login.perfil == EnumUsuarioPerfil.Cliente)
      params = params.set("UsuarioId", this.login.id);

    if (this.login.perfil == EnumUsuarioPerfil.Estabelecimento)
      params = params.set("EstabelecimentoId", this.login.estabelecimentoId.toString());

    this.pedidoService.get({ params })
      .then((data: any) => {
        this.isLoadingResults = false;
        this.isError = false;
        this.data = data.listaItens.map(x => new Pedido(x));
      })
      .catch(() => {
        this.isLoadingResults = false;
        this.isError = true;
      });
  }

  update(id: number, status) {
    this.pedidoService.getById(id)
      .then((data) => {
        data.status = status;
        this.pedidoService.save(data)
          .then(() => this.get());
      });
  }

  getValueEnumPedidoStatus = (value: number) => UtilService.getValueEnumPedidoStatus(value);
}
