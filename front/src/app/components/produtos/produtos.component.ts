import { HttpParams } from '@angular/common/http';
import { Component } from '@angular/core';
import { ProdutoService } from 'src/app/api-services/produtoService';
import { EnumPedidoStatus } from 'src/app/models/enums/enumPedidoStatus';
import { EnumUsuarioPerfil } from 'src/app/models/enums/enumUsuarioPerfil';
import { Login } from 'src/app/models/login';
import { Produto } from 'src/app/models/produto';
import { SessionService } from 'src/app/services/session.service';

@Component({
  selector: 'app-produtos',
  templateUrl: './produtos.component.html',
  styleUrls: ['./produtos.component.scss']
})
export class ProdutosComponent {
  login: Login;

  displayedColumns: string[] = ['nome', 'categoria', 'subcategoria', 'quantidade', 'tamanho', 'preco', 'acoes'];
  data: Produto[] = [];

  isLoadingResults = true;
  isError = false;

  constructor(
    private produtoService: ProdutoService,
    private sessionService: SessionService
  ) {
    this.login = this.sessionService.getLogin();
    if (this.login.perfil === EnumUsuarioPerfil.Admin)
      this.displayedColumns = ['estabelecimento'].concat(this.displayedColumns);

  }

  ngAfterViewInit() {
    this.get();
  }

  get() {
    this.isLoadingResults = true;
    let params = new HttpParams();

    if (this.login.perfil == EnumUsuarioPerfil.Admin)
      params = params.set("Includes", "Estabelecimento");

    if (this.login.perfil == EnumUsuarioPerfil.Estabelecimento)
      params = params.set("EstabelecimentoId", this.login.estabelecimentoId.toString());

    this.produtoService.get({ params })
      .then((data: any) => {
        this.isLoadingResults = false;
        this.isError = false;
        this.data = data.listaItens.map(x => new Produto(x));
      })
      .catch(() => {
        this.isLoadingResults = false;
        this.isError = true;
      });
  }

  delete(id: number) {
    this.produtoService.delete(id)
      .then(() => this.get());
  }
}
