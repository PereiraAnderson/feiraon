import { Component } from '@angular/core';
import { EstabelecimentoService } from 'src/app/api-services/estabelecimentoService';
import { Estabelecimento } from 'src/app/models/estabelecimento';

@Component({
  selector: 'app-estabelecimentos',
  templateUrl: './estabelecimentos.component.html',
  styleUrls: ['./estabelecimentos.component.scss']
})
export class EstabelecimentosComponent {

  displayedColumns: string[] = ['nome', 'endereco', 'telefone', 'email', 'acoes'];
  data: Estabelecimento[] = [];

  isLoadingResults = true;
  isError = false;

  constructor(private estabelecimentoService: EstabelecimentoService) { }

  ngAfterViewInit() {
    this.get();
  }

  get() {
    this.isLoadingResults = true;
    this.estabelecimentoService.get()
      .then((data: any) => {
        this.isLoadingResults = false;
        this.isError = false;
        this.data = data.listaItens.map(x => new Estabelecimento(x));
      })
      .catch(() => {
        this.isLoadingResults = false;
        this.isError = true;
      });
  }

  delete(id: number) {
    this.estabelecimentoService.delete(id)
      .then(() => this.get());
  }

}
