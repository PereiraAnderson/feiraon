import { HttpParams } from '@angular/common/http';
import { Component } from '@angular/core';
import { EstoqueService } from 'src/app/api-services/estoqueService';
import { EnumUsuarioPerfil } from 'src/app/models/enums/enumUsuarioPerfil';
import { Estoque } from 'src/app/models/estoque';
import { Login } from 'src/app/models/login';
import { SessionService } from 'src/app/services/session.service';

@Component({
  selector: 'app-estoques',
  templateUrl: './estoques.component.html',
  styleUrls: ['./estoques.component.scss']
})
export class EstoquesComponent {

  displayedColumns: string[] = ['produto', 'quantidade', 'acoes'];
  data: Estoque[] = [];

  isLoadingResults = true;
  isError = false;

  login: Login;

  constructor(
    private estoqueService: EstoqueService,
    private sessionService: SessionService
  ) {
    this.login = this.sessionService.getLogin();
    if (this.login.perfil === EnumUsuarioPerfil.Admin)
      this.displayedColumns = ['estabelecimento'].concat(this.displayedColumns);
  }

  ngAfterViewInit() {
    this.get();
  }

  get() {
    this.isLoadingResults = true;
    let params = new HttpParams()
      .set("Includes", "Estabelecimento")
      .append("Includes", "Produto");

    if (this.login.perfil === EnumUsuarioPerfil.Estabelecimento)
      params = params.set("EstabelecimentoId", this.login.estabelecimentoId.toString());

    this.estoqueService.get({ params })
      .then((data: any) => {
        this.isLoadingResults = false;
        this.isError = false;
        this.data = data.listaItens.map(x => new Estoque(x));
      })
      .catch(() => {
        this.isLoadingResults = false;
        this.isError = true;
      });
  }

  delete(id: number) {
    this.estoqueService.delete(id)
      .then(() => this.get());
  }
}
