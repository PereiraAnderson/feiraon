import { HttpParams } from '@angular/common/http';
import { Component } from '@angular/core';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { EstabelecimentoService } from 'src/app/api-services/estabelecimentoService';
import { PedidoService } from 'src/app/api-services/pedidoService';
import { ProdutoService } from 'src/app/api-services/produtoService';
import { EnumPedidoStatus } from 'src/app/models/enums/enumPedidoStatus';
import { Estabelecimento } from 'src/app/models/estabelecimento';
import { Login } from 'src/app/models/login';
import { Pedido } from 'src/app/models/pedido';
import { Produto } from 'src/app/models/produto';
import { ProdutoPedido } from 'src/app/models/produtoPedido';
import { SessionService } from 'src/app/services/session.service';

@Component({
  selector: 'app-pedido',
  templateUrl: './pedido.component.html',
  styleUrls: ['./pedido.component.scss']
})
export class PedidoComponent {
  pedido: Pedido;
  acao: string;
  estabelecimentos: Estabelecimento[];
  produtos: Produto[];
  produtoId: number;
  quantidade: number;
  valorTotal: number;
  displayedColumns: string[] = ['produto', 'quantidade', 'acoes'];
  login: Login;

  prodsSubCat: Produto[];
  prodsCat: Produto[];

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private pedidoService: PedidoService,
    private estabelecimentoService: EstabelecimentoService,
    private produtoService: ProdutoService,
    private sessions: SessionService
  ) {
    this.pedido = new Pedido();
    this.pedido.produtos = [];
    this.login = this.sessions.getLogin();
  }

  ngOnInit() {
    this.estabelecimentoService.get()
      .then((data: any) => {
        this.estabelecimentos = data.listaItens.map(x => new Estabelecimento(x));
      })

    this.route.params.subscribe((params: Params) => {
      const id = params['id'];
      if (id != 0) {
        const params = new HttpParams()
          .set('Includes', 'Produtos.Produto')
          .append('Includes', 'Usuario')
          .append('Includes', 'Estabelecimento');

        this.pedidoService.getById(id, { params })
          .then((data) => {
            this.pedido = new Pedido(data)
            this.calculaValorTotal();
          });
        this.acao = 'Visualizar';
      }
      else {
        this.acao = 'Criar';
      }
    });
  }

  onChangeEstab() {
    const params = new HttpParams()
      .set("EstabelecimentoId", this.pedido.estabelecimentoId.toString())
    this.produtoService.get({ params })
      .then((data: any) => {
        this.produtos = data.listaItens.map(x => new Produto(x));
      })

    this.prodsCat = [];
    this.prodsSubCat = [];

    this.valorTotal = 0;
    this.pedido.produtos = [];
  }

  onChangeProd() {
    const p = this.produtos.find(x => x.id == this.produtoId);

    let params = new HttpParams()
      .set("EstabelecimentoId", this.pedido.estabelecimentoId.toString())
      .set("Categoria", p.categoria)
      .set("Tamanho", "2")
      .set("ListaTodos", "false");

    this.produtoService.get({ params })
      .then((data: any) => {
        this.prodsCat = data.listaItens.map(x => new Produto(x));
      })

    params = new HttpParams()
      .set("Subcategoria", p.subcategoria)
      .set("Tamanho", "2")
      .set("ListaTodos", "false");

    this.produtoService.get({ params })
      .then((data: any) => {
        this.prodsSubCat = data.listaItens.map(x => new Produto(x));
      })
  }

  onSubmit() {
    this.pedido.data = new Date();
    this.pedido.status = EnumPedidoStatus["Não lido"];
    this.pedido.produtos.forEach(x => x.produto = undefined);
    this.pedido.usuarioId = this.login.id;

    this.pedidoService.save(this.pedido)
      .then(() => this.router.navigate(['/pedidos']));
  }

  avaliar() {
    this.pedido.produtos = undefined;

    this.pedidoService.save(this.pedido)
      .then(() => this.router.navigate(['/pedidos']));
  }

  adicionar() {
    const p = this.produtos.find(x => x.id == this.produtoId);
    this.pedido.produtos.push(
      new ProdutoPedido({
        produtoId: p.id,
        produto: { nome: p.nome, preco: p.preco },
        quantidade: this.quantidade
      }));
    this.calculaValorTotal();
  }

  delete(id: number) {
    const index = this.pedido.produtos.findIndex(x => x.produtoId == id);
    this.pedido.produtos.splice(index, 1);
    this.calculaValorTotal();
  }

  calculaValorTotal() {
    this.valorTotal = 0;
    this.pedido.produtos.forEach(x => this.valorTotal += +x.quantidade * +x.produto.preco);
  }
}
