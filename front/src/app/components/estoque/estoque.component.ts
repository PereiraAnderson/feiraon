import { HttpParams } from '@angular/common/http';
import { Component } from '@angular/core';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { EstabelecimentoService } from 'src/app/api-services/estabelecimentoService';
import { EstoqueService } from 'src/app/api-services/estoqueService';
import { ProdutoService } from 'src/app/api-services/produtoService';
import { EnumUsuarioPerfil } from 'src/app/models/enums/enumUsuarioPerfil';
import { Estabelecimento } from 'src/app/models/estabelecimento';
import { Estoque } from 'src/app/models/estoque';
import { Login } from 'src/app/models/login';
import { Produto } from 'src/app/models/produto';
import { SessionService } from 'src/app/services/session.service';

@Component({
  selector: 'app-estoque',
  templateUrl: './estoque.component.html',
  styleUrls: ['./estoque.component.scss']
})
export class EstoqueComponent {
  estoque: Estoque;
  acao: string;
  login: Login;
  produtos: Produto[];
  estabelecimentos: Estabelecimento[];

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private produtoService: ProdutoService,
    private estabelecimentoService: EstabelecimentoService,
    private estoqueService: EstoqueService,
    private sessionService: SessionService
  ) {
    this.login = this.sessionService.getLogin();
    this.estoque = new Estoque();
  }

  ngOnInit() {
    if (this.login.perfil == EnumUsuarioPerfil.Admin)
      this.estabelecimentoService.get()
        .then((data: any) => {
          this.estabelecimentos = data.listaItens.map(x => new Estabelecimento(x));
        })

    let params = new HttpParams();
    if (this.login.perfil == EnumUsuarioPerfil.Estabelecimento)
      params = params.set("EstabelecimentoId", this.login.estabelecimentoId.toString());

    this.produtoService.get({ params })
      .then((data: any) => {
        this.produtos = data.listaItens.map(x => new Produto(x));
      })

    this.route.params.subscribe((params: Params) => {
      const id = params['id'];
      if (id != 0) {
        this.estoqueService.getById(id)
          .then((data) => {
            this.estoque = new Estoque(data)
          });
        this.acao = 'Editar';
      }
      else {
        this.acao = 'Criar';
      }
    });
  }

  onChangeEstab() {
    const params = new HttpParams()
      .set("EstabelecimentoId", this.estoque.estabelecimentoId.toString())

    this.produtoService.get({ params })
      .then((data: any) => {
        this.produtos = data.listaItens.map(x => new Produto(x));
      })
  }

  onSubmit() {
    if (this.login.perfil == EnumUsuarioPerfil.Estabelecimento)
      this.estoque.estabelecimentoId = this.login.estabelecimentoId;

    this.estoqueService.save(this.estoque)
      .then(() => this.router.navigate(['/estoques']));
  }
}
