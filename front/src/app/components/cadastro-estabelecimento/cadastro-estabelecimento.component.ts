import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { EstabelecimentoService } from 'src/app/api-services/estabelecimentoService';
import { UsuarioService } from 'src/app/api-services/usuarioService';
import { EnumUsuarioPerfil } from 'src/app/models/enums/enumUsuarioPerfil';
import { Estabelecimento } from 'src/app/models/estabelecimento';
import { Usuario } from 'src/app/models/usuario';

@Component({
  selector: 'app-cadastro-estabelecimento',
  templateUrl: './cadastro-estabelecimento.component.html',
  styleUrls: ['./cadastro-estabelecimento.component.scss']
})
export class CadastroEstabelecimentoComponent implements OnInit {
  estabelecimento: Estabelecimento;
  usuario: Usuario;

  constructor(
    private router: Router,
    private estabelecimentoService: EstabelecimentoService,
    private usuarioService: UsuarioService
  ) {
    this.usuario = new Usuario();
    this.estabelecimento = new Estabelecimento();
  }

  ngOnInit() {

  }

  onSubmit() {
    this.estabelecimentoService.save(this.estabelecimento)
      .then((est) => {
        this.usuario.estabelecimentoId = est.id;
        this.usuario.perfil = EnumUsuarioPerfil.Estabelecimento;
        this.usuarioService.save(this.usuario)
          .then(() => this.router.navigate(['/login']));
      });
  }
}
