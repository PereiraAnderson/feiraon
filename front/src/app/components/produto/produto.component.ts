import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { EstabelecimentoService } from 'src/app/api-services/estabelecimentoService';
import { ProdutoService } from 'src/app/api-services/produtoService';
import { EnumUsuarioPerfil } from 'src/app/models/enums/enumUsuarioPerfil';
import { Estabelecimento } from 'src/app/models/estabelecimento';
import { Login } from 'src/app/models/login';
import { Produto } from 'src/app/models/produto';
import { SessionService } from 'src/app/services/session.service';

@Component({
  selector: 'app-produto',
  templateUrl: './produto.component.html',
  styleUrls: ['./produto.component.scss']
})
export class ProdutoComponent implements OnInit {
  produto: Produto;
  estabelecimentos: Estabelecimento[];
  acao: string;
  login: Login;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private produtoService: ProdutoService,
    private estabelecimentoService: EstabelecimentoService,
    private sessionService: SessionService) {
    this.login = this.sessionService.getLogin();
    this.produto = new Produto();
    this.estabelecimentos = [];
  }

  ngOnInit() {
    this.estabelecimentoService.get()
      .then((data) => {
        this.estabelecimentos = data.listaItens.map(x => new Estabelecimento(x));
      })

    this.route.params.subscribe((params: Params) => {
      const id = params['id'];
      if (id && id != 0) {
        this.produtoService.getById(id)
          .then((data) => {
            this.produto = new Produto(data)
          });
        this.acao = 'Editar';
      }
      else {
        this.acao = 'Criar';
      }
    });
  }

  foto(event) {
    let me = this;
    let file = event.target.files[0];
    let reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = function () {
      me.produto.foto = reader.result.toString();
      console.log(reader.result);
    };
    reader.onerror = function (error) {
      console.log('Error: ', error);
    };
  }

  onSubmit() {
    if (this.login.perfil == EnumUsuarioPerfil.Estabelecimento)
      this.produto.estabelecimentoId = this.login.estabelecimentoId;

    this.produtoService.save(this.produto)
      .then(() => this.router.navigate(['/produtos']));
  }
}
