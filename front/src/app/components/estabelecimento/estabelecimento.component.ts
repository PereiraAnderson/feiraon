import { Component } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { EstabelecimentoService } from 'src/app/api-services/estabelecimentoService';
import { Estabelecimento } from 'src/app/models/estabelecimento';

@Component({
  selector: 'app-estabelecimento',
  templateUrl: './estabelecimento.component.html',
  styleUrls: ['./estabelecimento.component.scss']
})
export class EstabelecimentoComponent {
  estabelecimento: Estabelecimento;
  acao: string;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private estabelecimentoService: EstabelecimentoService) {

    this.estabelecimento = new Estabelecimento();
  }

  ngOnInit() {
    this.route.params.subscribe((params: Params) => {
      const id = params['id'];
      if (id != 0) {
        this.estabelecimentoService.getById(id)
          .then((data) => {
            this.estabelecimento = new Estabelecimento(data)
          });
        this.acao = 'Editar';
      }
      else {
        this.acao = 'Criar';
      }
    });
  }

  onSubmit() {
    this.estabelecimentoService.save(this.estabelecimento)
      .then(() => this.router.navigate(['/estabelecimentos']));
  }
}
