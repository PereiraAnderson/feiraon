import { HttpParams } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { EstoqueService } from 'src/app/api-services/estoqueService';
import { EnumUsuarioPerfil } from 'src/app/models/enums/enumUsuarioPerfil';
import { Estoque } from 'src/app/models/estoque';
import { Login } from 'src/app/models/login';
import { SessionService } from 'src/app/services/session.service';

@Component({
  selector: 'app-relatorio-estoque',
  templateUrl: './relatorio-estoque.component.html',
  styleUrls: ['./relatorio-estoque.component.scss']
})
export class RelatorioEstoqueComponent implements OnInit {

  displayedColumns: string[] = ['nome', 'categoria', 'subcategoria', 'quantidade', 'tamanho', 'precoU', 'precoT'];
  data: any[] = [];

  isLoadingResults = true;
  isError = false;
  somatorio: number;

  login: Login;

  constructor(
    private estoqueService: EstoqueService,
    private sessionService: SessionService
  ) {
    this.login = this.sessionService.getLogin();
  }

  ngOnInit() {
    this.get();
  }

  get() {
    this.isLoadingResults = true;
    const params = new HttpParams()
      .set("Includes", "Produto")
      .set("EstabelecimentoId", this.login.estabelecimentoId.toString());

    this.estoqueService.get({ params })
      .then((data: any) => {
        this.somatorio = 0;
        this.isLoadingResults = false;
        this.isError = false;
        const estoques = data.listaItens.map(x => new Estoque(x));
        this.data = estoques.map(x => {
          let final = {
            nome: x.produto.nome,
            categoria: x.produto.categoria,
            subcategoria: x.produto.subcategoria,
            tamanho: x.produto.tamanho,
            precoU: x.produto.preco,
            quantidade: x.quantidade,
            precoT: +x.produto.preco * +x.quantidade
          };
          this.somatorio += final.precoT;
          return final;
        })
      })
      .catch((e) => {
        console.log(e);

        this.isLoadingResults = false;
        this.isError = true;
      });
  }
}
