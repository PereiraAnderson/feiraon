import { HttpParams } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { PedidoService } from 'src/app/api-services/pedidoService';
import { Login } from 'src/app/models/login';
import { Pedido } from 'src/app/models/pedido';
import { SessionService } from 'src/app/services/session.service';

@Component({
  selector: 'app-relatorio-avaliacao',
  templateUrl: './relatorio-avaliacao.component.html',
  styleUrls: ['./relatorio-avaliacao.component.scss']
})
export class RelatorioAvaliacaoComponent implements OnInit {

  pedidos: Pedido[];
  login: Login;

  qntNota1: number = 0;
  qntNota2: number = 0;
  qntNota3: number = 0;
  qntNota4: number = 0;
  qntNota5: number = 0;

  comentsNota1: string[] = [];
  comentsNota2: string[] = [];
  comentsNota3: string[] = [];
  comentsNota4: string[] = [];
  comentsNota5: string[] = [];

  constructor(
    private pedidoService: PedidoService,
    private sessions: SessionService
  ) {
    this.login = this.sessions.getLogin();
  }

  ngOnInit(): void {
    const params = new HttpParams()
      .set("EstabelecimentoId", this.login.estabelecimentoId.toString());

    this.pedidoService.get({ params })
      .then((data: any) => {
        this.pedidos = data.listaItens.map(x => new Pedido(x));

        this.pedidos.forEach(p => {
          switch (p.avaliacao) {
            case 1:
              this.qntNota1++;
              if (p.comentario && p.comentario != '')
                this.comentsNota1.push(p.comentario);
              break;
            case 2:
              this.qntNota2++;
              if (p.comentario && p.comentario != '')
                this.comentsNota2.push(p.comentario);
              break;
            case 3:
              this.qntNota3++;
              if (p.comentario && p.comentario != '')
                this.comentsNota3.push(p.comentario);
              break;
            case 4:
              this.qntNota4++;
              if (p.comentario && p.comentario != '')
                this.comentsNota4.push(p.comentario);
              break;
            case 5:
              this.qntNota5++;
              if (p.comentario && p.comentario != '')
                this.comentsNota5.push(p.comentario);
              break;
          }
        });
      })
  }

}
