import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RelatorioAvaliacaoComponent } from './relatorio-avaliacao.component';

describe('RelatorioAvaliacaoComponent', () => {
  let component: RelatorioAvaliacaoComponent;
  let fixture: ComponentFixture<RelatorioAvaliacaoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RelatorioAvaliacaoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RelatorioAvaliacaoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
