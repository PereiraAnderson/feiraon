import { HttpParams } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { PedidoService } from 'src/app/api-services/pedidoService';
import { EnumPedidoStatus } from 'src/app/models/enums/enumPedidoStatus';
import { Login } from 'src/app/models/login';
import { Pedido } from 'src/app/models/pedido';
import { SessionService } from 'src/app/services/session.service';

@Component({
  selector: 'app-relatorio-venda',
  templateUrl: './relatorio-venda.component.html',
  styleUrls: ['./relatorio-venda.component.scss']
})
export class RelatorioVendaComponent implements OnInit {
  displayedColumns: string[] = ['data', 'produto', 'tamanho', 'quantidade', 'precoU', 'precoT'];
  data: any[] = [];

  isLoadingResults = true;
  isError = false;
  somatorio: number;

  login: Login;

  constructor(
    private pedidoService: PedidoService,
    private sessionService: SessionService
  ) {
    this.login = this.sessionService.getLogin();
  }

  ngOnInit() {
    this.get();
  }

  get() {
    this.isLoadingResults = true;
    const params = new HttpParams()
      .set("Includes", "Produtos.Produto")
      .set("EstabelecimentoId", this.login.estabelecimentoId.toString());

    this.pedidoService.get({ params })
      .then((data: any) => {
        this.somatorio = 0;
        this.isLoadingResults = false;
        this.isError = false;
        const pedidos: Pedido[] = data.listaItens.map(x => new Pedido(x));
        this.data = [];
        pedidos.map(ped => {
          if (ped.status == EnumPedidoStatus.Finalizado)
            ped.produtos.map(prod => {
              let final = {
                data: ped.data,
                produto: prod.produto.nome,
                tamanho: prod.produto.tamanho,
                quantidade: prod.quantidade,
                precoU: prod.produto.preco,
                precoT: +prod.produto.preco * +prod.quantidade
              };
              this.somatorio += final.precoT;
              this.data.push(final);
            })
        })
      })
      .catch((e) => {
        console.log(e);

        this.isLoadingResults = false;
        this.isError = true;
      });
  }
}
