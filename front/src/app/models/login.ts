export class Login {
    login: string;
    senha: string;

    id: string;
    nome: string;
    nomeEstabelecimento: string;
    estabelecimentoId: number;
    perfil: number;
    token: string;

    constructor(item?) {
        if (item) {
            this.id = item.id;
            this.nome = item.nome;
            this.nomeEstabelecimento = item.nomeEstabelecimento;
            this.estabelecimentoId = item.estabelecimentoId;
            this.perfil = item.perfil;
            this.token = item.bearer;
        }
    }
}
