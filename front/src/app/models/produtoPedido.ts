import { Pedido } from './pedido';
import { Produto } from './produto';

export class ProdutoPedido {
    quantidade: number;

    produtoId: number;
    produto: Produto;

    pedidoId: number;
    // pedido: Pedido;

    constructor(item?) {
        if (item) {
            this.quantidade = item.quantidade;

            this.produtoId = item.produtoId;
            this.pedidoId = item.pedidoId;

            if (item.produto)
                this.produto = new Produto(item.produto);

            // if (item.pedido)
            // this.pedido = new Pedido(item.pedido);
        }
    }
}
