import { EnumUsuarioPerfil } from './enums/enumUsuarioPerfil';
import { Estabelecimento } from './estabelecimento';
import { GenericModel } from './genericModel';

export class Usuario extends GenericModel {
    nome: string;
    cpf: string;
    rg: string;
    endereco: string;
    telefone: string;
    email: string;
    perfil: EnumUsuarioPerfil;
    login: string;
    senha: string;

    estabelecimentoId: number;
    estabelecimento: Estabelecimento;

    constructor(item?) {
        super(item);

        if (item) {
            this.nome = item.nome;
            this.cpf = item.cpf;
            this.rg = item.rg;
            this.endereco = item.endereco;
            this.telefone = item.telefone;
            this.email = item.email;
            this.perfil = item.perfil;
            this.login = item.login;
            this.senha = item.senha;

            this.estabelecimentoId = item.estabelecimentoId;

            if (item.estabelecimento)
                this.estabelecimento = new Estabelecimento(item.estabelecimento);
        }
    }
}
