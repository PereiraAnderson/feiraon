import { EnumPedidoStatus } from './enums/enumPedidoStatus';
import { Estabelecimento } from './estabelecimento';
import { GenericModel } from './genericModel';
import { ProdutoPedido } from './produtoPedido';
import { Usuario } from './usuario';

export class Pedido extends GenericModel {
    data: Date;
    status: EnumPedidoStatus;
    endereco: string;
    avaliacao: number;
    comentario: string;

    usuarioId: string;
    usuario: Usuario;

    estabelecimentoId: number;
    estabelecimento: Estabelecimento;

    produtos: ProdutoPedido[];

    constructor(item?) {
        super(item);

        if (item) {
            this.data = item.data;
            this.status = item.status;
            this.endereco = item.endereco;
            this.avaliacao = item.avaliacao;
            this.comentario = item.comentario;

            this.usuarioId = item.usuarioId;
            this.estabelecimentoId = item.estabelecimentoId;

            if (item.usuario)
                this.usuario = new Usuario(item.usuario);

            if (item.estabelecimento)
                this.estabelecimento = new Estabelecimento(item.estabelecimento);

            if (item.produtos)
                this.produtos = item.produtos.map(x => new ProdutoPedido(x));
        }
    }
}
