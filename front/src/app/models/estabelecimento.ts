import { GenericModel } from './genericModel';

export class Estabelecimento extends GenericModel {
    nome: string;
    endereco: string;
    telefone: string;
    email: string;

    constructor(item?) {
        super(item);

        if (item) {
            this.nome = item.nome;
            this.endereco = item.endereco;
            this.telefone = item.telefone;
            this.email = item.email;
        }
    }
}
