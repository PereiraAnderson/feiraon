import { Estabelecimento } from './estabelecimento';
import { GenericModel } from './genericModel';

export class Produto extends GenericModel {
    nome: string;
    categoria: string;
    subcategoria: string;
    quantidade: string;
    tamanho: string;
    foto: string;
    preco: number;

    estabelecimentoId: number;
    estabelecimento: Estabelecimento;

    constructor(item?) {
        super(item);

        if (item) {
            this.nome = item.nome;
            this.categoria = item.categoria;
            this.subcategoria = item.subcategoria;
            this.quantidade = item.quantidade;
            this.tamanho = item.tamanho;
            this.foto = item.foto;
            this.preco = item.preco;

            this.estabelecimentoId = item.estabelecimentoId;

            if (item.estabelecimento)
                this.estabelecimento = new Estabelecimento(item.estabelecimento);
        }
    }
}
