export enum EnumPedidoStatus {
    'Não lido' = 1,
    'Lido' = 2,
    'Em Entrega' = 3,
    'Finalizado' = 4,
    'Cancelado' = 5
}
