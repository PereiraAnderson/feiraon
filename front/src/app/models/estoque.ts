import { Estabelecimento } from './estabelecimento';
import { GenericModel } from './genericModel';
import { Produto } from './produto';

export class Estoque extends GenericModel {
    quantidade: number;

    produtoId: number;
    produto: Produto;

    estabelecimentoId: number;
    estabelecimento: Estabelecimento;

    constructor(item?) {
        super(item);

        if (item) {
            this.quantidade = item.quantidade;

            this.produtoId = item.produtoId;
            this.estabelecimentoId = item.estabelecimentoId;

            if (item.produto)
                this.produto = new Produto(item.produto);

            if (item.estabelecimento)
                this.estabelecimento = new Estabelecimento(item.estabelecimento);
        }
    }
}
