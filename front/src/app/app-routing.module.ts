import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CadastroEstabelecimentoComponent } from './components/cadastro-estabelecimento/cadastro-estabelecimento.component';
import { CadastroUsuarioComponent } from './components/cadastro-usuario/cadastro-usuario.component';
import { EstabelecimentoComponent } from './components/estabelecimento/estabelecimento.component';
import { EstabelecimentosComponent } from './components/estabelecimentos/estabelecimentos.component';
import { EstoqueComponent } from './components/estoque/estoque.component';
import { EstoquesComponent } from './components/estoques/estoques.component';
import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from './components/login/login.component';
import { PedidoComponent } from './components/pedido/pedido.component';
import { PedidosComponent } from './components/pedidos/pedidos.component';
import { ProdutoComponent } from './components/produto/produto.component';
import { ProdutosComponent } from './components/produtos/produtos.component';
import { UsuarioComponent } from './components/usuario/usuario.component';
import { UsuariosComponent } from './components/usuarios/usuarios.component';
import { RelatorioAvaliacaoComponent } from './relatorios/relatorio-avaliacao/relatorio-avaliacao.component';
import { RelatorioEstoqueComponent } from './relatorios/relatorio-estoque/relatorio-estoque.component';
import { RelatorioVendaComponent } from './relatorios/relatorio-venda/relatorio-venda.component';
import { AuthGuardService } from './services/auth-guard.service';

const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'cadastro-usuario', component: CadastroUsuarioComponent },
  { path: 'cadastro-estabelecimento', component: CadastroEstabelecimentoComponent },
  { path: '', component: HomeComponent, canActivate: [AuthGuardService] },
  { path: 'home', component: HomeComponent, canActivate: [AuthGuardService] },
  { path: 'usuarios', component: UsuariosComponent, canActivate: [AuthGuardService] },
  { path: 'usuarios/:id', component: UsuarioComponent, canActivate: [AuthGuardService] },
  { path: 'estabelecimentos', component: EstabelecimentosComponent, canActivate: [AuthGuardService] },
  { path: 'estabelecimentos/:id', component: EstabelecimentoComponent, canActivate: [AuthGuardService] },
  { path: 'pedidos', component: PedidosComponent, canActivate: [AuthGuardService] },
  { path: 'pedidos/:id', component: PedidoComponent, canActivate: [AuthGuardService] },
  { path: 'produtos', component: ProdutosComponent, canActivate: [AuthGuardService] },
  { path: 'produtos/:id', component: ProdutoComponent, canActivate: [AuthGuardService] },
  { path: 'estoques', component: EstoquesComponent, canActivate: [AuthGuardService] },
  { path: 'estoques/:id', component: EstoqueComponent, canActivate: [AuthGuardService] },
  { path: 'relatorio-venda', component: RelatorioVendaComponent, canActivate: [AuthGuardService] },
  { path: 'relatorio-estoque', component: RelatorioEstoqueComponent, canActivate: [AuthGuardService] },
  { path: 'relatorio-avaliacao', component: RelatorioAvaliacaoComponent, canActivate: [AuthGuardService] }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
