using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using FeiraON.Context.Configurations;
using FeiraON.Context.Models;

namespace FeiraON.Context
{
    public class FeiraONDbContext : IdentityDbContext<Usuario>
    {
        public DbSet<Estabelecimento> Estabelecimento { get; set; }
        public DbSet<Estoque> Estoque { get; set; }
        public DbSet<Pedido> Pedido { get; set; }
        public DbSet<Produto> Produto { get; set; }
        public DbSet<Usuario> Usuario { get; set; }

        public FeiraONDbContext(DbContextOptions options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            // Ordenados
            builder.ApplyConfiguration(new UsuarioConfiguration());
            builder.ApplyConfiguration(new AspNetUserClaimsConfiguration());

            // Não ordenados
            builder.ApplyConfiguration(new ProdutoPedidoConfiguration());
            builder.ApplyConfiguration(new EstoqueConfiguration());
        }
    }
}
