using System.IO;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;

namespace FeiraON.Context
{
    public class DesignTimeDbContextFactory :
        IDesignTimeDbContextFactory<FeiraONDbContext>
    {
        public FeiraONDbContext CreateDbContext(string[] args)
        {
            IConfigurationRoot configuration = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.Migration.json")
            .Build();

            var builder = new DbContextOptionsBuilder<FeiraONDbContext>();

            var connectionString = configuration.GetConnectionString("SqlServerFeiraON");

            builder.UseSqlServer(connectionString);

            return new FeiraONDbContext(builder.Options);
        }
    }
}
