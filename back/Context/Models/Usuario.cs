using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using FeiraON.Infra.Enums;
using Microsoft.AspNetCore.Identity;

namespace FeiraON.Context.Models
{
    public class Usuario : IdentityUser
    {
        [Required]
        public bool Ativo { get; set; }
        [Required]
        public DateTimeOffset DataCriacao { get; set; }
        public DateTimeOffset? DataModificacao { get; set; }

        [Required]
        public string Nome { get; set; }

        [Required]
        public string CPF { get; set; }

        [Required]
        public string RG { get; set; }

        [Required]
        public string Endereco { get; set; }

        [NotMapped]
        public EnumUsuarioPerfil Perfil { get; set; }

        public long? EstabelecimentoId { get; set; }
        public Estabelecimento Estabelecimento { get; set; }
    }
}
