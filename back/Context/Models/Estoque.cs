using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FeiraON.Context.Models
{
    public class Estoque : GenericModel
    {
        [Required]
        public double Quantidade { get; set; }

        [Required]
        [ForeignKey("ProdutoId")]
        public long ProdutoId { get; set; }
        public Produto Produto { get; set; }

        [Required]
        [ForeignKey("EstabelecimentoId")]
        public long EstabelecimentoId { get; set; }
        public Estabelecimento Estabelecimento { get; set; }

    }
}
