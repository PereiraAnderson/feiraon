using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using FeiraON.Infra.Enums;

namespace FeiraON.Context.Models
{
    public class Pedido : GenericModel
    {
        [Required]
        public DateTimeOffset Data { get; set; }

        [Required]
        public EnumPedidoStatus Status { get; set; }

        [Required]
        public string Endereco { get; set; }

        public int Avaliacao { get; set; }

        public string Comentario { get; set; }

        [Required]
        [ForeignKey("UsuarioId")]
        public string UsuarioId { get; set; }
        public Usuario Usuario { get; set; }

        [Required]
        [ForeignKey("EstabelecimentoId")]
        public long EstabelecimentoId { get; set; }
        public Estabelecimento Estabelecimento { get; set; }

        public IEnumerable<ProdutoPedido> Produtos { get; set; }
    }
}
