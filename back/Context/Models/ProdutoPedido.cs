using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FeiraON.Context.Models
{
    public class ProdutoPedido
    {
        [Required]
        public double Quantidade { get; set; }

        [Required]
        public long ProdutoId { get; set; }
        public Produto Produto { get; set; }

        [Required]
        public long PedidoId { get; set; }
        public Pedido Pedido { get; set; }
    }
}
