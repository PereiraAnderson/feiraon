using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FeiraON.Context.Models
{
    public class Produto : GenericModel
    {
        [Required]
        public string Nome { get; set; }

        [Required]
        public string Categoria { get; set; }

        [Required]
        public string Subcategoria { get; set; }

        [Required]
        public string Quantidade { get; set; }

        [Required]
        public string Tamanho { get; set; }

        public string Foto { get; set; }

        [Required]
        public double Preco { get; set; }

        [Required]
        [ForeignKey("EstabelecimentoId")]
        public long EstabelecimentoId { get; set; }
        public Estabelecimento Estabelecimento { get; set; }
    }
}
