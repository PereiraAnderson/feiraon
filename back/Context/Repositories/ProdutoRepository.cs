using System.Linq;
using FeiraON.Context.Models;
using FeiraON.Context.Repositories.Interfaces;
using FeiraON.Extensions;
using FeiraON.Infra.Filters;
using FeiraON.Infra.Utils;

namespace FeiraON.Context.Repositories
{
    public class ProdutoRepository : FeiraONRepository<Produto>, IProdutoRepository
    {

        public ProdutoRepository(FeiraONDbContext context) : base(context)
        {
        }

        public IOrderedQueryable<Produto> Get(ProdutoFiltro filtro = null, Ordenacao ordenacao = null) =>
             GetAll().AplicaFiltro(filtro).AplicaOrdenacao(ordenacao);

        public Paginacao<Produto> Get(Paginacao paginacao, ProdutoFiltro filtro = null, Ordenacao ordenacao = null) =>
            new Paginacao<Produto>(Get(filtro, ordenacao), paginacao);
    }
}