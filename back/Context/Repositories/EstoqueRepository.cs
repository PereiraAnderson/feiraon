using System.Linq;
using FeiraON.Context.Models;
using FeiraON.Context.Repositories.Interfaces;
using FeiraON.Extensions;
using FeiraON.Infra.Filters;
using FeiraON.Infra.Utils;

namespace FeiraON.Context.Repositories
{
    public class EstoqueRepository : FeiraONRepository<Estoque>, IEstoqueRepository
    {

        public EstoqueRepository(FeiraONDbContext context) : base(context)
        {
        }

        public IOrderedQueryable<Estoque> Get(EstoqueFiltro filtro = null, Ordenacao ordenacao = null) =>
             GetAll().AplicaFiltro(filtro).AplicaOrdenacao(ordenacao);

        public Paginacao<Estoque> Get(Paginacao paginacao, EstoqueFiltro filtro = null, Ordenacao ordenacao = null) =>
            new Paginacao<Estoque>(Get(filtro, ordenacao), paginacao);
    }
}