using System.Linq;
using FeiraON.Context.Models;
using FeiraON.Context.Repositories.Interfaces;
using FeiraON.Extensions;
using FeiraON.Infra.Filters;
using FeiraON.Infra.Utils;

namespace FeiraON.Context.Repositories
{
    public class EstabelecimentoRepository : FeiraONRepository<Estabelecimento>, IEstabelecimentoRepository
    {

        public EstabelecimentoRepository(FeiraONDbContext context) : base(context)
        {
        }

        public IOrderedQueryable<Estabelecimento> Get(EstabelecimentoFiltro filtro = null, Ordenacao ordenacao = null) =>
             GetAll().AplicaFiltro(filtro).AplicaOrdenacao(ordenacao);

        public Paginacao<Estabelecimento> Get(Paginacao paginacao, EstabelecimentoFiltro filtro = null, Ordenacao ordenacao = null) =>
            new Paginacao<Estabelecimento>(Get(filtro, ordenacao), paginacao);
    }
}