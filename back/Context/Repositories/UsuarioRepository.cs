using System.Linq;
using FeiraON.Context.Models;
using FeiraON.Context.Repositories.Interfaces;
using FeiraON.Extensions;
using FeiraON.Infra.Filters;
using FeiraON.Infra.Utils;

namespace FeiraON.Context.Repositories
{
    public class UsuarioRepository : FeiraONRepository<Usuario>, IUsuarioRepository
    {

        public UsuarioRepository(FeiraONDbContext context) : base(context)
        {
        }

        public IOrderedQueryable<Usuario> Get(UsuarioFiltro filtro = null, Ordenacao ordenacao = null) =>
            GetAll().AplicaFiltro(filtro).AplicaOrdenacao(ordenacao);

        public Paginacao<Usuario> Get(Paginacao paginacao, UsuarioFiltro filtro = null, Ordenacao ordenacao = null) =>
            new Paginacao<Usuario>(Get(filtro, ordenacao), paginacao);
    }
}