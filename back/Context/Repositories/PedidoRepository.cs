using System.Linq;
using FeiraON.Context.Models;
using FeiraON.Context.Repositories.Interfaces;
using FeiraON.Extensions;
using FeiraON.Infra.Filters;
using FeiraON.Infra.Utils;

namespace FeiraON.Context.Repositories
{
    public class PedidoRepository : FeiraONRepository<Pedido>, IPedidoRepository
    {

        public PedidoRepository(FeiraONDbContext context) : base(context)
        {
        }

        public IOrderedQueryable<Pedido> Get(PedidoFiltro filtro = null, Ordenacao ordenacao = null) =>
             GetAll().AplicaFiltro(filtro).AplicaOrdenacao(ordenacao);

        public Paginacao<Pedido> Get(Paginacao paginacao, PedidoFiltro filtro = null, Ordenacao ordenacao = null) =>
            new Paginacao<Pedido>(Get(filtro, ordenacao), paginacao);
    }
}