using System.Linq;
using FeiraON.Context.Models;
using FeiraON.Infra.Filters;
using FeiraON.Infra.Utils;

namespace FeiraON.Context.Repositories.Interfaces
{
    public interface IEstoqueRepository : IFeiraONRepository<Estoque>
    {
        IOrderedQueryable<Estoque> Get(EstoqueFiltro filtro = null, Ordenacao ordenacao = null);
        Paginacao<Estoque> Get(Paginacao paginacao, EstoqueFiltro filtro = null, Ordenacao ordenacao = null);
    }
}