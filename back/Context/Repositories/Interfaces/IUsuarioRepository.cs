using System.Linq;
using FeiraON.Context.Models;
using FeiraON.Infra.Filters;
using FeiraON.Infra.Utils;

namespace FeiraON.Context.Repositories.Interfaces
{
    public interface IUsuarioRepository : IFeiraONRepository<Usuario>
    {
        IOrderedQueryable<Usuario> Get(UsuarioFiltro filtro = null, Ordenacao ordenacao = null);
        Paginacao<Usuario> Get(Paginacao paginacao, UsuarioFiltro filtro = null, Ordenacao ordenacao = null);
    }
}