using System.Linq;
using FeiraON.Context.Models;
using FeiraON.Infra.Filters;
using FeiraON.Infra.Utils;

namespace FeiraON.Context.Repositories.Interfaces
{
    public interface IEstabelecimentoRepository : IFeiraONRepository<Estabelecimento>
    {
        IOrderedQueryable<Estabelecimento> Get(EstabelecimentoFiltro filtro = null, Ordenacao ordenacao = null);
        Paginacao<Estabelecimento> Get(Paginacao paginacao, EstabelecimentoFiltro filtro = null, Ordenacao ordenacao = null);
    }
}