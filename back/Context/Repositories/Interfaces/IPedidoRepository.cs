using System.Linq;
using FeiraON.Context.Models;
using FeiraON.Infra.Filters;
using FeiraON.Infra.Utils;

namespace FeiraON.Context.Repositories.Interfaces
{
    public interface IPedidoRepository : IFeiraONRepository<Pedido>
    {
        IOrderedQueryable<Pedido> Get(PedidoFiltro filtro = null, Ordenacao ordenacao = null);
        Paginacao<Pedido> Get(Paginacao paginacao, PedidoFiltro filtro = null, Ordenacao ordenacao = null);
    }
}