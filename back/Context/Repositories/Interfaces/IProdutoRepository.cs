using System.Linq;
using FeiraON.Context.Models;
using FeiraON.Infra.Filters;
using FeiraON.Infra.Utils;

namespace FeiraON.Context.Repositories.Interfaces
{
    public interface IProdutoRepository : IFeiraONRepository<Produto>
    {
        IOrderedQueryable<Produto> Get(ProdutoFiltro filtro = null, Ordenacao ordenacao = null);
        Paginacao<Produto> Get(Paginacao paginacao, ProdutoFiltro filtro = null, Ordenacao ordenacao = null);
    }
}