using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using FeiraON.Context.Models;
using FeiraON.Context.Repositories.Interfaces;
using FeiraON.Infra.Enums;
using FeiraON.Infra.Filters;
using FeiraON.Infra.Utils;
using FeiraON.Infra.Views;
using FeiraON.Infra.Views.Models;
using FeiraON.Services.Interfaces;

namespace FeiraON.Services
{
    public class UsuarioService : IUsuarioService
    {
        private readonly IUsuarioRepository _repo;
        private readonly IServiceProvider _serviceProvider;
        private IConfiguration _configuration;
        private SignInManager<Usuario> _signInManager;
        private UserManager<Usuario> _userManager;

        public UsuarioService(IUsuarioRepository repo, IServiceProvider serviceProvider)
        {
            _repo = repo;
            _serviceProvider = serviceProvider;
        }

        public IEnumerable<Usuario> Get(UsuarioFiltro filtro = null, Ordenacao ordenacao = null) =>
            _repo.Get(filtro, ordenacao).AsEnumerable();

        public Paginacao<Usuario> Get(Paginacao paginacao, UsuarioFiltro filtro = null, Ordenacao ordenacao = null) =>
            _repo.Get(paginacao, filtro, ordenacao);

        public Usuario Get(string key, IEnumerable<string> includes = null)
        {
            var user = _repo.Get(key, includes);
            _signInManager ??= _serviceProvider.GetRequiredService<SignInManager<Usuario>>();
            var claims = _signInManager.CreateUserPrincipalAsync(user).Result;
            user.Perfil = (EnumUsuarioPerfil)Enum.Parse(typeof(EnumUsuarioPerfil), claims.Claims.First(x => x.Type.Equals(ClaimTypes.Role)).Value);
            return user;
        }

        public async Task<Usuario> Add(Usuario usuario)
        {
            usuario.Id = Guid.NewGuid().ToString();
            usuario.DataCriacao = DateTimeOffset.Now;
            usuario.Ativo = true;

            _userManager ??= _serviceProvider.GetRequiredService<UserManager<Usuario>>();

            var ret = await _userManager.CreateAsync(usuario, usuario.PasswordHash);
            if (!ret.Succeeded)
                throw new Exception(string.Concat(ret.Errors.Select(x => x.Description)));

            Claim claim = new Claim(ClaimTypes.Role, usuario.Perfil.ToString());
            await _userManager.AddClaimAsync(usuario, claim);

            return usuario;
        }

        public Usuario Update(UsuarioView usuarioView)
        {
            _userManager ??= _serviceProvider.GetRequiredService<UserManager<Usuario>>();
            var usuario = _userManager.FindByIdAsync(usuarioView.Id).Result;
            if (usuario == null)
                throw new Exception("Id inexistente");

            usuario.DataModificacao = DateTimeOffset.Now;
            usuario.Nome = usuarioView.Nome;
            usuario.CPF = usuarioView.CPF;
            usuario.RG = usuarioView.RG;
            usuario.Email = usuarioView.Email;
            usuario.PhoneNumber = usuarioView.Telefone;
            usuario.Endereco = usuarioView.Endereco;

            var ret = _userManager.UpdateAsync(usuario).Result;
            if (!ret.Succeeded)
                throw new Exception(string.Concat(ret.Errors.Select(x => x.Description)));

            return usuario;
        }

        public Usuario Remove(Usuario usuario)
        {
            var ret = _repo.Remove(usuario);
            _repo.SaveChanges();
            return ret;
        }

        public Usuario Remove(string id)
        {
            var ret = _repo.Remove(id);
            _repo.SaveChanges();
            return ret;
        }

        public async Task<LoginOut> Login(LoginIn login)
        {
            _signInManager ??= _serviceProvider.GetRequiredService<SignInManager<Usuario>>();

            var usuario = Get(new UsuarioFiltro
            {
                Login = login.Login,
                Includes = new string[] { "Estabelecimento" }
            }).SingleOrDefault();

            if (usuario == null)
                throw new Exception("E-mail e/ou senha incorreta");

            var result = await _signInManager.CheckPasswordSignInAsync(usuario, login.Senha, false);

            if (!result.Succeeded)
                throw new Exception("E-mail e/ou senha incorreta");

            JwtSecurityToken token = CreateToken(usuario);
            var jwtSecurityToken = new JwtSecurityTokenHandler().WriteToken(token);

            return new LoginOut
            {
                Id = usuario.Id,
                Nome = usuario.Nome,
                NomeEstabelecimento = usuario.Estabelecimento?.Nome,
                EstabelecimentoId = usuario.EstabelecimentoId,
                Perfil = (EnumUsuarioPerfil)Enum.Parse(typeof(EnumUsuarioPerfil), token.Claims.First(x => x.Type.Equals(ClaimTypes.Role)).Value),
                Token = jwtSecurityToken
            };
        }

        public Usuario AlterarSenha(AlterarSenha alterarSenha)
        {
            _userManager ??= _serviceProvider.GetRequiredService<UserManager<Usuario>>();

            var usuario = _userManager.FindByNameAsync(alterarSenha.Email).Result;
            if (usuario == null)
                throw new Exception("E-mail inexistente");

            var ret = _userManager.ChangePasswordAsync(usuario, alterarSenha.SenhaAtual, alterarSenha.SenhaNova).Result;
            if (!ret.Succeeded)
                throw new Exception(string.Concat(ret.Errors.Select(x => x.Description)));

            usuario.DataModificacao = DateTimeOffset.Now;
            ret = _userManager.UpdateAsync(usuario).Result;
            if (!ret.Succeeded)
                throw new Exception(string.Concat(ret.Errors.Select(x => x.Description)));

            return usuario;
        }

        private JwtSecurityToken CreateToken(Usuario usuario)
        {
            _configuration ??= _serviceProvider.GetRequiredService<IConfiguration>();

            var claims = _signInManager.CreateUserPrincipalAsync(usuario).Result;

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["Autentication:SecurityKey"]));
            var credentials = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            var token = new JwtSecurityToken(
                issuer: _configuration["Autentication:Issue"],
                audience: _configuration["Autentication:Audience"],
                claims: claims.Claims,
                signingCredentials: credentials
            );
            return token;
        }
    }
}