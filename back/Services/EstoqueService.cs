using System.Collections.Generic;
using System.Linq;
using FeiraON.Context.Models;
using FeiraON.Context.Repositories.Interfaces;
using FeiraON.Infra.Filters;
using FeiraON.Infra.Utils;
using FeiraON.Services.Interfaces;

namespace FeiraON.Services
{
    public class EstoqueService : IEstoqueService
    {
        private readonly IEstoqueRepository _repo;

        public EstoqueService(IEstoqueRepository repo)
        {
            _repo = repo;
        }

        public IEnumerable<Estoque> Get(EstoqueFiltro filtro = null, Ordenacao ordenacao = null) =>
            _repo.Get(filtro, ordenacao).AsEnumerable();

        public Paginacao<Estoque> Get(Paginacao paginacao, EstoqueFiltro filtro = null, Ordenacao ordenacao = null) =>
            _repo.Get(paginacao, filtro, ordenacao);

        public Estoque Get(long id, IEnumerable<string> includes = null) =>
            _repo.Get(id, includes);

        public Estoque Add(Estoque estoque)
        {
            var ret = _repo.Add(estoque);
            _repo.SaveChanges();
            return ret;
        }

        public void Add(IEnumerable<Estoque> estoques)
        {
            _repo.Add(estoques);
            _repo.SaveChanges();
        }

        public Estoque Update(Estoque estoque)
        {
            var ret = _repo.Update(estoque);
            _repo.SaveChanges();
            return ret;
        }

        public void Update(IEnumerable<Estoque> estoques)
        {
            _repo.Update(estoques);
            _repo.SaveChanges();
        }

        public Estoque Remove(Estoque estoque)
        {
            var ret = _repo.Remove(estoque);
            _repo.SaveChanges();
            return ret;
        }

        public void Remove(IEnumerable<Estoque> estoques)
        {
            _repo.Remove(estoques);
            _repo.SaveChanges();
        }

        public Estoque Remove(long id)
        {
            var ret = _repo.Remove(id);
            _repo.SaveChanges();
            return ret;
        }

        public void Remove(IEnumerable<dynamic> ids)
        {
            _repo.Remove(ids);
            _repo.SaveChanges();
        }
    }
}