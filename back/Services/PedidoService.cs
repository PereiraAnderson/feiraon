using System.Collections.Generic;
using System.Linq;
using FeiraON.Context.Models;
using FeiraON.Context.Repositories.Interfaces;
using FeiraON.Infra.Filters;
using FeiraON.Infra.Utils;
using FeiraON.Services.Interfaces;

namespace FeiraON.Services
{
    public class PedidoService : IPedidoService
    {
        private readonly IPedidoRepository _repo;

        public PedidoService(IPedidoRepository repo)
        {
            _repo = repo;
        }

        public IEnumerable<Pedido> Get(PedidoFiltro filtro = null, Ordenacao ordenacao = null) =>
            _repo.Get(filtro, ordenacao).AsEnumerable();

        public Paginacao<Pedido> Get(Paginacao paginacao, PedidoFiltro filtro = null, Ordenacao ordenacao = null) =>
            _repo.Get(paginacao, filtro, ordenacao);

        public Pedido Get(long id, IEnumerable<string> includes = null) =>
            _repo.Get(id, includes);

        public Pedido Add(Pedido pedido)
        {
            var ret = _repo.Add(pedido);
            _repo.SaveChanges();
            return ret;
        }

        public void Add(IEnumerable<Pedido> pedidos)
        {
            _repo.Add(pedidos);
            _repo.SaveChanges();
        }

        public Pedido Update(Pedido pedido)
        {
            var ret = _repo.Update(pedido);
            _repo.SaveChanges();
            return ret;
        }

        public void Update(IEnumerable<Pedido> pedidos)
        {
            _repo.Update(pedidos);
            _repo.SaveChanges();
        }

        public Pedido Remove(Pedido pedido)
        {
            var ret = _repo.Remove(pedido);
            _repo.SaveChanges();
            return ret;
        }

        public void Remove(IEnumerable<Pedido> pedidos)
        {
            _repo.Remove(pedidos);
            _repo.SaveChanges();
        }

        public Pedido Remove(long id)
        {
            var ret = _repo.Remove(id);
            _repo.SaveChanges();
            return ret;
        }

        public void Remove(IEnumerable<dynamic> ids)
        {
            _repo.Remove(ids);
            _repo.SaveChanges();
        }
    }
}