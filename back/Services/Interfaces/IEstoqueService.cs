using System.Collections.Generic;
using FeiraON.Context.Models;
using FeiraON.Infra.Filters;
using FeiraON.Infra.Utils;

namespace FeiraON.Services.Interfaces
{
    public interface IEstoqueService
    {
        IEnumerable<Estoque> Get(EstoqueFiltro filtro = null, Ordenacao ordenacao = null);
        Paginacao<Estoque> Get(Paginacao paginacao, EstoqueFiltro filtro = null, Ordenacao ordenacao = null);
        Estoque Get(long id, IEnumerable<string> includes = null);
        Estoque Add(Estoque estoque);
        void Add(IEnumerable<Estoque> estoques);
        Estoque Update(Estoque estoque);
        void Update(IEnumerable<Estoque> estoques);
        Estoque Remove(Estoque estoque);
        void Remove(IEnumerable<Estoque> estoques);
        Estoque Remove(long id);
        void Remove(IEnumerable<dynamic> ids);
    }
}