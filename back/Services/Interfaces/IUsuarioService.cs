using System.Collections.Generic;
using System.Threading.Tasks;
using FeiraON.Context.Models;
using FeiraON.Infra.Filters;
using FeiraON.Infra.Utils;
using FeiraON.Infra.Views;
using FeiraON.Infra.Views.Models;

namespace FeiraON.Services.Interfaces
{
    public interface IUsuarioService
    {
        IEnumerable<Usuario> Get(UsuarioFiltro filtro = null, Ordenacao ordenacao = null);
        Paginacao<Usuario> Get(Paginacao paginacao, UsuarioFiltro filtro = null, Ordenacao ordenacao = null);
        Usuario Get(string id, IEnumerable<string> includes = null);
        Task<Usuario> Add(Usuario usuario);
        Usuario Update(UsuarioView usuarioView);
        Usuario Remove(Usuario usuario);
        Usuario Remove(string id);
        Task<LoginOut> Login(LoginIn login);
        Usuario AlterarSenha(AlterarSenha alteraSenha);
    }
}