using System.Collections.Generic;
using FeiraON.Context.Models;
using FeiraON.Infra.Filters;
using FeiraON.Infra.Utils;

namespace FeiraON.Services.Interfaces
{
    public interface IEstabelecimentoService
    {
        IEnumerable<Estabelecimento> Get(EstabelecimentoFiltro filtro = null, Ordenacao ordenacao = null);
        Paginacao<Estabelecimento> Get(Paginacao paginacao, EstabelecimentoFiltro filtro = null, Ordenacao ordenacao = null);
        Estabelecimento Get(long id, IEnumerable<string> includes = null);
        Estabelecimento Add(Estabelecimento estabelecimento);
        void Add(IEnumerable<Estabelecimento> estabelecimentos);
        Estabelecimento Update(Estabelecimento estabelecimento);
        void Update(IEnumerable<Estabelecimento> estabelecimentos);
        Estabelecimento Remove(Estabelecimento estabelecimento);
        void Remove(IEnumerable<Estabelecimento> estabelecimentos);
        Estabelecimento Remove(long id);
        void Remove(IEnumerable<dynamic> ids);
    }
}