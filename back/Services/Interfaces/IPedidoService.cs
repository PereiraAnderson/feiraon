using System.Collections.Generic;
using FeiraON.Context.Models;
using FeiraON.Infra.Filters;
using FeiraON.Infra.Utils;

namespace FeiraON.Services.Interfaces
{
    public interface IPedidoService
    {
        IEnumerable<Pedido> Get(PedidoFiltro filtro = null, Ordenacao ordenacao = null);
        Paginacao<Pedido> Get(Paginacao paginacao, PedidoFiltro filtro = null, Ordenacao ordenacao = null);
        Pedido Get(long id, IEnumerable<string> includes = null);
        Pedido Add(Pedido pedido);
        void Add(IEnumerable<Pedido> pedidos);
        Pedido Update(Pedido pedido);
        void Update(IEnumerable<Pedido> pedidos);
        Pedido Remove(Pedido pedido);
        void Remove(IEnumerable<Pedido> pedidos);
        Pedido Remove(long id);
        void Remove(IEnumerable<dynamic> ids);
    }
}