using System.Collections.Generic;
using FeiraON.Context.Models;
using FeiraON.Infra.Filters;
using FeiraON.Infra.Utils;

namespace FeiraON.Services.Interfaces
{
    public interface IProdutoService
    {
        IEnumerable<Produto> Get(ProdutoFiltro filtro = null, Ordenacao ordenacao = null);
        Paginacao<Produto> Get(Paginacao paginacao, ProdutoFiltro filtro = null, Ordenacao ordenacao = null);
        Produto Get(long id, IEnumerable<string> includes = null);
        Produto Add(Produto produto);
        void Add(IEnumerable<Produto> produtos);
        Produto Update(Produto produto);
        void Update(IEnumerable<Produto> produtos);
        Produto Remove(Produto produto);
        void Remove(IEnumerable<Produto> produtos);
        Produto Remove(long id);
        void Remove(IEnumerable<dynamic> ids);
    }
}