using System.Collections.Generic;
using System.Linq;
using FeiraON.Context.Models;
using FeiraON.Context.Repositories.Interfaces;
using FeiraON.Infra.Filters;
using FeiraON.Infra.Utils;
using FeiraON.Services.Interfaces;

namespace FeiraON.Services
{
    public class EstabelecimentoService : IEstabelecimentoService
    {
        private readonly IEstabelecimentoRepository _repo;

        public EstabelecimentoService(IEstabelecimentoRepository repo)
        {
            _repo = repo;
        }

        public IEnumerable<Estabelecimento> Get(EstabelecimentoFiltro filtro = null, Ordenacao ordenacao = null) =>
            _repo.Get(filtro, ordenacao).AsEnumerable();

        public Paginacao<Estabelecimento> Get(Paginacao paginacao, EstabelecimentoFiltro filtro = null, Ordenacao ordenacao = null) =>
            _repo.Get(paginacao, filtro, ordenacao);

        public Estabelecimento Get(long id, IEnumerable<string> includes = null) =>
            _repo.Get(id, includes);

        public Estabelecimento Add(Estabelecimento estabelecimento)
        {
            var ret = _repo.Add(estabelecimento);
            _repo.SaveChanges();
            return ret;
        }

        public void Add(IEnumerable<Estabelecimento> estabelecimentos)
        {
            _repo.Add(estabelecimentos);
            _repo.SaveChanges();
        }

        public Estabelecimento Update(Estabelecimento estabelecimento)
        {
            var ret = _repo.Update(estabelecimento);
            _repo.SaveChanges();
            return ret;
        }

        public void Update(IEnumerable<Estabelecimento> estabelecimentos)
        {
            _repo.Update(estabelecimentos);
            _repo.SaveChanges();
        }

        public Estabelecimento Remove(Estabelecimento estabelecimento)
        {
            var ret = _repo.Remove(estabelecimento);
            _repo.SaveChanges();
            return ret;
        }

        public void Remove(IEnumerable<Estabelecimento> estabelecimentos)
        {
            _repo.Remove(estabelecimentos);
            _repo.SaveChanges();
        }

        public Estabelecimento Remove(long id)
        {
            var ret = _repo.Remove(id);
            _repo.SaveChanges();
            return ret;
        }

        public void Remove(IEnumerable<dynamic> ids)
        {
            _repo.Remove(ids);
            _repo.SaveChanges();
        }
    }
}