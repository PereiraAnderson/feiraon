using System.Collections.Generic;
using System.Linq;
using FeiraON.Context.Models;
using FeiraON.Context.Repositories.Interfaces;
using FeiraON.Infra.Filters;
using FeiraON.Infra.Utils;
using FeiraON.Services.Interfaces;

namespace FeiraON.Services
{
    public class ProdutoService : IProdutoService
    {
        private readonly IProdutoRepository _repo;

        public ProdutoService(IProdutoRepository repo)
        {
            _repo = repo;
        }

        public IEnumerable<Produto> Get(ProdutoFiltro filtro = null, Ordenacao ordenacao = null) =>
            _repo.Get(filtro, ordenacao).AsEnumerable();

        public Paginacao<Produto> Get(Paginacao paginacao, ProdutoFiltro filtro = null, Ordenacao ordenacao = null) =>
            _repo.Get(paginacao, filtro, ordenacao);

        public Produto Get(long id, IEnumerable<string> includes = null) =>
            _repo.Get(id, includes);

        public Produto Add(Produto produto)
        {
            var ret = _repo.Add(produto);
            _repo.SaveChanges();
            return ret;
        }

        public void Add(IEnumerable<Produto> produtos)
        {
            _repo.Add(produtos);
            _repo.SaveChanges();
        }

        public Produto Update(Produto produto)
        {
            var ret = _repo.Update(produto);
            _repo.SaveChanges();
            return ret;
        }

        public void Update(IEnumerable<Produto> produtos)
        {
            _repo.Update(produtos);
            _repo.SaveChanges();
        }

        public Produto Remove(Produto produto)
        {
            var ret = _repo.Remove(produto);
            _repo.SaveChanges();
            return ret;
        }

        public void Remove(IEnumerable<Produto> produtos)
        {
            _repo.Remove(produtos);
            _repo.SaveChanges();
        }

        public Produto Remove(long id)
        {
            var ret = _repo.Remove(id);
            _repo.SaveChanges();
            return ret;
        }

        public void Remove(IEnumerable<dynamic> ids)
        {
            _repo.Remove(ids);
            _repo.SaveChanges();
        }
    }
}