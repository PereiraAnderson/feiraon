namespace FeiraON.Infra.Filters
{
    public class EstoqueFiltro : GenericFilter
    {
        public string Categoria { get; set; }
        public string Subcategoria { get; set; }
        public long? EstabelecimentoId { get; set; }
    }
}
