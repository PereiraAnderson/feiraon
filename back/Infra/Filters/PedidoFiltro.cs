using System;
using FeiraON.Infra.Enums;

namespace FeiraON.Infra.Filters
{
    public class PedidoFiltro : GenericFilter
    {
        public DateTimeOffset? Data { get; set; }
        public string Nome { get; set; }
        public string CPF { get; set; }
        public EnumPedidoStatus? Status { get; set; }
        public string UsuarioId { get; set; }
        public long? EstabelecimentoId { get; set; }
    }
}
