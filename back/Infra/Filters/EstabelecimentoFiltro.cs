namespace FeiraON.Infra.Filters
{
    public class EstabelecimentoFiltro : GenericFilter
    {
        public string Nome { get; set; }
        public string Endereco { get; set; }
        public string Telefone { get; set; }
        public string Email { get; set; }
    }
}
