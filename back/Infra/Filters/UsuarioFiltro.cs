namespace FeiraON.Infra.Filters
{
    public class UsuarioFiltro : GenericFilter
    {
        public string Telefone { get; set; }
        public string Nome { get; set; }
        public string Login { get; set; }
    }
}
