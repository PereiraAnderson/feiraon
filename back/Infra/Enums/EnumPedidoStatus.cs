namespace FeiraON.Infra.Enums
{
    public enum EnumPedidoStatus
    {
        NaoLido = 1,
        Lido = 2,
        EmEntrega = 3,
        Finalizado = 4,
        Cancelado = 5
    }
}