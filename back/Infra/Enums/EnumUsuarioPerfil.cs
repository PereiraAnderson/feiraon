namespace FeiraON.Infra.Enums
{
    public enum EnumUsuarioPerfil
    {
        ADMIN = 1,
        ESTABELECIMENTO = 2,
        CLIENTE = 3
    }
}