namespace FeiraON.Infra.Views
{
    public class AlterarSenha
    {
        public string Email { get; set; }
        public string Token { get; set; }
        public string SenhaAtual { get; set; }
        public string SenhaNova { get; set; }
    }
}