using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using FeiraON.Infra.Enums;

namespace FeiraON.Infra.Views.Models
{
    public class UsuarioView
    {
        public string Id { get; set; }
        public bool Ativo { get; set; }
        public DateTimeOffset DataCriacao { get; set; }
        public DateTimeOffset? DataModificacao { get; set; }

        [Required]
        public string Nome { get; set; }

        [Required]
        public string CPF { get; set; }

        [Required]
        public string RG { get; set; }

        [Required]
        public string Endereco { get; set; }

        [Required]
        public string Telefone { get; set; }

        [Required]
        public string Email { get; set; }

        [Required]
        public EnumUsuarioPerfil Perfil { get; set; }

        [Required]
        public string Login { get; set; }

        public string Senha { get; set; }

        [ForeignKey("EstabelecimentoId")]
        public long? EstabelecimentoId { get; set; }
        public EstabelecimentoView Estabelecimento { get; set; }

    }
}
