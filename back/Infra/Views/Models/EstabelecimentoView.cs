using System.ComponentModel.DataAnnotations;

namespace FeiraON.Infra.Views.Models
{
    public class EstabelecimentoView : GenericView
    {
        [Required]
        public string Nome { get; set; }

        [Required]
        public string Endereco { get; set; }

        [Required]
        public string Telefone { get; set; }

        public string Email { get; set; }
    }
}
