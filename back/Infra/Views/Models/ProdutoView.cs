using System.ComponentModel.DataAnnotations;

namespace FeiraON.Infra.Views.Models
{
    public class ProdutoView : GenericView
    {
        [Required]
        public string Nome { get; set; }

        [Required]
        public string Categoria { get; set; }

        [Required]
        public string Subcategoria { get; set; }

        [Required]
        public string Quantidade { get; set; }

        [Required]
        public string Tamanho { get; set; }

        public string Foto { get; set; }

        [Required]
        public double Preco { get; set; }


        [Required]
        public long EstabelecimentoId { get; set; }
        public EstabelecimentoView Estabelecimento { get; set; }
    }
}
