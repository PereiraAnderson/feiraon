using System.ComponentModel.DataAnnotations;

namespace FeiraON.Infra.Views.Models
{
    public class EstoqueView : GenericView
    {
        [Required]
        public double Quantidade { get; set; }

        [Required]
        public long ProdutoId { get; set; }
        public ProdutoView Produto { get; set; }

        [Required]
        public long EstabelecimentoId { get; set; }
        public EstabelecimentoView Estabelecimento { get; set; }
    }
}
