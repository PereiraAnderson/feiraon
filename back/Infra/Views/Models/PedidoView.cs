using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using FeiraON.Infra.Enums;

namespace FeiraON.Infra.Views.Models
{
    public class PedidoView : GenericView
    {
        [Required]
        public DateTimeOffset Data { get; set; }

        [Required]
        public EnumPedidoStatus Status { get; set; }

        [Required]
        public string Endereco { get; set; }

        public int Avaliacao { get; set; }

        public string Comentario { get; set; }

        [Required]
        public string UsuarioId { get; set; }
        public UsuarioView Usuario { get; set; }

        [Required]
        public long EstabelecimentoId { get; set; }
        public EstabelecimentoView Estabelecimento { get; set; }

        public IEnumerable<ProdutoPedidoView> Produtos { get; set; }
    }
}
