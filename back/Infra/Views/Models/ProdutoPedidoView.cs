using System.ComponentModel.DataAnnotations;

namespace FeiraON.Infra.Views.Models
{
    public class ProdutoPedidoView
    {
        [Required]
        public double Quantidade { get; set; }

        [Required]
        public long ProdutoId { get; set; }
        public ProdutoView Produto { get; set; }

        [Required]
        public long PedidoId { get; set; }
        public PedidoView Pedido { get; set; }
    }
}
