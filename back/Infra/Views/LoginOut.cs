using FeiraON.Infra.Enums;

namespace FeiraON.Infra.Views
{
    public class LoginOut
    {
        public string Id { get; set; }
        public string Nome { get; set; }
        public string NomeEstabelecimento { get; set; }
        public long? EstabelecimentoId { get; set; }
        public EnumUsuarioPerfil Perfil { get; set; }
        public string Token { get; set; }
    }
}