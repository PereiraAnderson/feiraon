using FeiraON.Context.Models;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.ResponseCompression;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using FeiraON.Context;
using Microsoft.Extensions.Logging;
using System;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using FeiraON.Services;
using FeiraON.Services.Interfaces;
using FeiraON.Context.Repositories.Interfaces;
using FeiraON.Context.Repositories;
using NSwag;
using System.Linq;

namespace FeiraON
{
    public class StartupMigration
    {
        public IConfiguration _configuration { get; }

        public StartupMigration(IConfiguration configuration, IWebHostEnvironment env) =>
            _configuration = configuration;

        public void ConfigureServices(IServiceCollection services) =>
            services.AddDbContext<FeiraONDbContext>(options =>
                options.UseSqlServer(_configuration.GetConnectionString("SqlServerFeiraON")));

        public void Configure(IApplicationBuilder app)
        {
        }
    }

    public class Startup
    {
        readonly IConfiguration _configuration;
        readonly IWebHostEnvironment _environment;
        readonly string MyAllowSpecificOrigins = "_myAllowSpecificOrigins";

        public Startup(IWebHostEnvironment environment, IConfiguration configuration)
        {
            _environment = environment;
            _configuration = configuration;
        }


        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddResponseCompression(options =>
            {
                options.Providers.Add<BrotliCompressionProvider>();
                options.Providers.Add<GzipCompressionProvider>();
            });

            services.AddSwaggerDocument(configure =>
            {
                configure.AddSecurity("JWT", Enumerable.Empty<string>(), new OpenApiSecurityScheme
                {
                    Type = OpenApiSecuritySchemeType.ApiKey,
                    Name = "Authorization",
                    In = OpenApiSecurityApiKeyLocation.Header,
                    Description = "Type into the textbox: Bearer {your JWT token}."
                });

                configure.PostProcess = document =>
                {
                    document.Info.Version = "v1";
                    document.Info.Title = "Swagger FeiraON";
                    document.Info.Description = "Swagger com todas as rotas acessíveis da FeiraON";
                    document.Info.TermsOfService = "None";
                };
            });

            services.AddCors(options =>
            {
                options.AddPolicy(MyAllowSpecificOrigins,
                builder =>
                {
                    builder.AllowAnyHeader()
                        .AllowAnyMethod()
                        .AllowAnyOrigin();
                });
            });

            services.AddAuthorization();
            services.AddControllers()
                .AddJsonOptions(option => option.JsonSerializerOptions.IgnoreNullValues = true);
            services.AddMvc();

            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options =>
               {
                   options.TokenValidationParameters = new TokenValidationParameters
                   {
                       ValidateIssuer = true,
                       ValidateAudience = true,
                       ValidateLifetime = false,
                       ValidateIssuerSigningKey = true,
                       IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["Autentication:SecurityKey"])),
                       ValidIssuer = _configuration["Autentication:Issue"],
                       ValidAudience = _configuration["Autentication:Audience"]
                   };
               });

            services.AddDbContext<FeiraONDbContext>(options =>
                options.UseSqlServer(_configuration.GetConnectionString("SqlServerFeiraON")));

            services.AddIdentityCore<Usuario>(options =>
                    {
                        options.Password.RequireDigit = false;
                        options.Password.RequireLowercase = false;
                        options.Password.RequireNonAlphanumeric = false;
                        options.Password.RequireUppercase = false;
                        options.Password.RequiredLength = 6;
                    })
                .AddEntityFrameworkStores<FeiraONDbContext>()
                .AddDefaultUI()
                .AddDefaultTokenProviders();

            services.AddHealthChecks();

            services.AddTransient<IEstabelecimentoRepository, EstabelecimentoRepository>();
            services.AddTransient<IProdutoRepository, ProdutoRepository>();
            services.AddTransient<IPedidoRepository, PedidoRepository>();
            services.AddTransient<IEstoqueRepository, EstoqueRepository>();
            services.AddTransient<IUsuarioRepository, UsuarioRepository>();

            services.AddTransient<IEstabelecimentoService, EstabelecimentoService>();
            services.AddTransient<IProdutoService, ProdutoService>();
            services.AddTransient<IPedidoService, PedidoService>();
            services.AddTransient<IEstoqueService, EstoqueService>();
            services.AddTransient<IUsuarioService, UsuarioService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IServiceProvider serviceProvider)
        {
            app.UseResponseCompression();
            app.UseRouting();
            app.UseCors(MyAllowSpecificOrigins);
            app.UseAuthentication();
            app.UseAuthorization();

            app.UseOpenApi();
            app.UseSwaggerUi3();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapHealthChecks("/");
                endpoints.MapHealthChecks("/health");
            });

            FeiraONDbContext context = serviceProvider.GetRequiredService<FeiraONDbContext>();
            context.Database.Migrate();

            var logger = serviceProvider.GetRequiredService<ILogger<Startup>>();
            logger.LogInformation("New FeiraON Instance.");
        }
    }
}
