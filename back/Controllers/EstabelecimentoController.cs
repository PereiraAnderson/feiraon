﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using FeiraON.Extensions;
using FeiraON.Infra.Filters;
using FeiraON.Infra.Utils;
using FeiraON.Infra.Views;
using FeiraON.Infra.Views.Models;
using FeiraON.Services.Interfaces;

namespace FeiraON.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class EstabelecimentoController : ControllerBase
    {
        private readonly ILogger<EstabelecimentoController> _logger;
        private readonly IEstabelecimentoService _service;

        public EstabelecimentoController(ILogger<EstabelecimentoController> logger, IEstabelecimentoService service)
        {
            _logger = logger;
            _service = service;
        }

        [HttpGet]

        public IActionResult GetEstabelecimentos([FromQuery] Paginacao paginacao, [FromQuery] EstabelecimentoFiltro filtro, [FromQuery] Ordenacao ordenacao)
        {
            try
            {
                var result = _service.Get(paginacao, filtro, ordenacao);
                return Ok(new Paginacao<EstabelecimentoView>
                {
                    TotalItens = result.TotalItens,
                    NumeroPagina = result.NumeroPagina,
                    TamanhoPagina = result.TamanhoPagina,
                    TotalPaginas = result.TotalPaginas,
                    ListaItens = result.ListaItens.Select(x => x.ToView())
                });
            }
            catch (Exception e)
            {
                return BadRequest(new Erro { Mensagem = e.Message, Detalhes = e.InnerException?.Message }); ;
            }
        }

        [HttpGet("{id}")]

        public IActionResult GetEstabelecimento([FromRoute] long id, [FromQuery] IEnumerable<string> includes)
        {
            try
            {
                var result = _service.Get(id, includes);
                return Ok(result.ToView());
            }
            catch (Exception e)
            {
                return BadRequest(new Erro { Mensagem = e.Message, Detalhes = e.InnerException?.Message }); ;
            }
        }

        [HttpPost]
        public IActionResult PostEstabelecimento([FromBody] EstabelecimentoView estabelecimento)
        {
            try
            {
                var result = _service.Add(estabelecimento.ToModel());
                return CreatedAtAction(nameof(GetEstabelecimento), new { id = result.Id }, result.ToView());
            }
            catch (Exception e)
            {
                return BadRequest(new Erro { Mensagem = e.Message, Detalhes = e.InnerException?.Message }); ;
            }
        }

        [HttpPut]

        public IActionResult PutEstabelecimento([FromBody] EstabelecimentoView estabelecimento)
        {
            try
            {
                var result = _service.Update(estabelecimento.ToModel());
                return Ok(result.ToView());
            }
            catch (Exception e)
            {
                return BadRequest(new Erro { Mensagem = e.Message, Detalhes = e.InnerException?.Message }); ;
            }
        }

        [HttpDelete("{id}")]

        public IActionResult DeleteEstabelecimento([FromRoute] long id)
        {
            try
            {
                var result = _service.Remove(id);
                return Ok(result.ToView());
            }
            catch (Exception e)
            {
                return BadRequest(new Erro { Mensagem = e.Message, Detalhes = e.InnerException?.Message }); ;
            }
        }
    }
}
