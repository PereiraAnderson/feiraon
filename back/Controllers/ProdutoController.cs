﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using FeiraON.Extensions;
using FeiraON.Infra.Filters;
using FeiraON.Infra.Utils;
using FeiraON.Infra.Views;
using FeiraON.Infra.Views.Models;
using FeiraON.Services.Interfaces;

namespace FeiraON.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ProdutoController : ControllerBase
    {
        private readonly ILogger<ProdutoController> _logger;
        private readonly IProdutoService _service;

        public ProdutoController(ILogger<ProdutoController> logger, IProdutoService service)
        {
            _logger = logger;
            _service = service;
        }

        [HttpGet]

        public IActionResult GetProdutos([FromQuery] Paginacao paginacao, [FromQuery] ProdutoFiltro filtro, [FromQuery] Ordenacao ordenacao)
        {
            try
            {
                var result = _service.Get(paginacao, filtro, ordenacao);
                return Ok(new Paginacao<ProdutoView>
                {
                    TotalItens = result.TotalItens,
                    NumeroPagina = result.NumeroPagina,
                    TamanhoPagina = result.TamanhoPagina,
                    TotalPaginas = result.TotalPaginas,
                    ListaItens = result.ListaItens.Select(x => x.ToView())
                });
            }
            catch (Exception e)
            {
                return BadRequest(new Erro { Mensagem = e.Message, Detalhes = e.InnerException?.Message }); ;
            }
        }

        [HttpGet("{id}")]

        public IActionResult GetProduto([FromRoute] long id, [FromQuery] IEnumerable<string> includes)
        {
            try
            {
                var result = _service.Get(id, includes);
                return Ok(result.ToView());
            }
            catch (Exception e)
            {
                return BadRequest(new Erro { Mensagem = e.Message, Detalhes = e.InnerException?.Message }); ;
            }
        }

        [HttpPost]

        public IActionResult PostProduto([FromBody] ProdutoView produto)
        {
            try
            {
                var result = _service.Add(produto.ToModel());
                return CreatedAtAction(nameof(GetProduto), new { id = result.Id }, result.ToView());
            }
            catch (Exception e)
            {
                return BadRequest(new Erro { Mensagem = e.Message, Detalhes = e.InnerException?.Message }); ;
            }
        }

        [HttpPut]

        public IActionResult PutProduto([FromBody] ProdutoView produto)
        {
            try
            {
                var result = _service.Update(produto.ToModel());
                return Ok(result.ToView());
            }
            catch (Exception e)
            {
                return BadRequest(new Erro { Mensagem = e.Message, Detalhes = e.InnerException?.Message }); ;
            }
        }

        [HttpDelete("{id}")]

        public IActionResult DeleteProduto([FromRoute] long id)
        {
            try
            {
                var result = _service.Remove(id);
                return Ok(result.ToView());
            }
            catch (Exception e)
            {
                return BadRequest(new Erro { Mensagem = e.Message, Detalhes = e.InnerException?.Message }); ;
            }
        }
    }
}
