﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using FeiraON.Extensions;
using FeiraON.Infra.Enums;
using FeiraON.Infra.Filters;
using FeiraON.Infra.Utils;
using FeiraON.Infra.Views;
using FeiraON.Infra.Views.Models;
using FeiraON.Services.Interfaces;

namespace FeiraON.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class UsuarioController : ControllerBase
    {
        private readonly ILogger<UsuarioController> _logger;
        private readonly IUsuarioService _service;

        public UsuarioController(ILogger<UsuarioController> logger, IUsuarioService service)
        {
            _logger = logger;
            _service = service;
        }

        [HttpGet]

        public IActionResult GetUsuarios([FromQuery] Paginacao paginacao, [FromQuery] UsuarioFiltro filtro, [FromQuery] Ordenacao ordenacao)
        {
            try
            {
                var result = _service.Get(paginacao, filtro, ordenacao);
                return Ok(new Paginacao<UsuarioView>
                {
                    TotalItens = result.TotalItens,
                    NumeroPagina = result.NumeroPagina,
                    TamanhoPagina = result.TamanhoPagina,
                    TotalPaginas = result.TotalPaginas,
                    ListaItens = result.ListaItens.Select(x => x.ToView())
                });
            }
            catch (Exception e)
            {
                return BadRequest(new Erro { Mensagem = e.Message, Detalhes = e.InnerException?.Message }); ;
            }
        }

        [HttpGet("{id}")]

        public IActionResult GetUsuario([FromRoute] string id, [FromQuery] IEnumerable<string> includes)
        {
            try
            {
                var result = _service.Get(id, includes);
                return Ok(result.ToView());
            }
            catch (Exception e)
            {
                return BadRequest(new Erro { Mensagem = e.Message, Detalhes = e.InnerException?.Message }); ;
            }
        }

        [HttpPost]
        public IActionResult PostUsuarios([FromBody] UsuarioView usuario)
        {
            var isAdmin = HttpContext.User.Claims.Any(x => x.Type == ClaimTypes.Role && x.Value.Equals("ADMIN"));
            if (!isAdmin && usuario.Perfil == EnumUsuarioPerfil.ADMIN)
                return BadRequest(new Erro { Mensagem = "Não é possível criar administradores" });

            try
            {
                var result = _service.Add(usuario.ToModel()).Result;
                return CreatedAtAction(nameof(GetUsuario), new { id = result.Id }, result.ToView());
            }
            catch (Exception e)
            {
                return BadRequest(new Erro { Mensagem = e.Message, Detalhes = e.InnerException?.Message });
            }
        }

        [HttpPut]

        public IActionResult PutUsuario([FromBody] UsuarioView usuario)
        {
            try
            {
                var result = _service.Update(usuario);
                return Ok(result.ToView());
            }
            catch (Exception e)
            {
                return BadRequest(new Erro { Mensagem = e.Message, Detalhes = e.InnerException?.Message }); ;
            }
        }

        [HttpPut("AlteraSenha")]

        public IActionResult PutUsuarioAlteraSenha([FromBody] AlterarSenha alterarSenha)
        {
            try
            {
                var result = _service.AlterarSenha(alterarSenha);
                return Ok(result.ToView());
            }
            catch (Exception e)
            {
                return BadRequest(new Erro { Mensagem = e.Message, Detalhes = e.InnerException?.Message }); ;
            }
        }

        [HttpDelete("{id}")]

        public IActionResult DeleteUsuarios([FromRoute] string id)
        {
            try
            {
                var result = _service.Remove(id);
                return Ok(result.ToView());
            }
            catch (Exception e)
            {
                return BadRequest(new Erro { Mensagem = e.Message, Detalhes = e.InnerException?.Message }); ;
            }
        }

        [HttpPost("Login")]
        public IActionResult Login([FromBody] LoginIn login)
        {
            try
            {
                var result = _service.Login(login).Result;
                return Ok(result);
            }
            catch (Exception e)
            {
                return BadRequest(new Erro { Mensagem = e.Message, Detalhes = e.InnerException?.Message }); ;
            }
        }
    }
}
