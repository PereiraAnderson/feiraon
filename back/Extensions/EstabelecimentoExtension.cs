using System.Linq;
using FeiraON.Context.Models;
using FeiraON.Infra.Filters;
using FeiraON.Infra.Views.Models;

namespace FeiraON.Extensions
{
    public static class EstabelecimentoExtension
    {
        public static Estabelecimento ToModel(this EstabelecimentoView view)
        {
            var model = new Estabelecimento
            {
                Nome = view.Nome,
                Email = view.Email,
                Endereco = view.Endereco,
                Telefone = view.Telefone
            };

            GenericViewExtension.ToModel(view, model);
            return model;
        }

        public static EstabelecimentoView ToView(this Estabelecimento model)
        {
            var view = new EstabelecimentoView
            {
                Nome = model.Nome,
                Email = model.Email,
                Endereco = model.Endereco,
                Telefone = model.Telefone
            };

            GenericViewExtension.ToView(model, view);
            return view;
        }

        public static IQueryable<T> AplicaFiltro<T>(this IQueryable<T> query, EstabelecimentoFiltro filtro)
            where T : Estabelecimento
        {
            if (filtro != null)
            {
                if (!string.IsNullOrWhiteSpace(filtro.Nome))
                    query = query.Where(x => x.Nome.Contains(filtro.Nome));

                if (!string.IsNullOrWhiteSpace(filtro.Endereco))
                    query = query.Where(x => x.Endereco.Contains(filtro.Endereco));

                if (!string.IsNullOrWhiteSpace(filtro.Telefone))
                    query = query.Where(x => x.Telefone.Contains(filtro.Telefone));

                if (!string.IsNullOrWhiteSpace(filtro.Email))
                    query = query.Where(x => x.Email.Contains(filtro.Email));

                query = query.AplicaGenericFilter(filtro);
            }

            return query;
        }
    }
}