using System.Linq;
using FeiraON.Context.Models;
using FeiraON.Infra.Filters;
using FeiraON.Infra.Views.Models;

namespace FeiraON.Extensions
{
    public static class ProdutoExtension
    {
        public static Produto ToModel(this ProdutoView view)
        {
            var model = new Produto
            {
                Categoria = view.Categoria,
                Foto = view.Foto,
                Nome = view.Nome,
                Preco = view.Preco,
                Subcategoria = view.Subcategoria,
                Tamanho = view.Tamanho,
                Quantidade = view.Quantidade,
                EstabelecimentoId = view.EstabelecimentoId
            };

            GenericViewExtension.ToModel(view, model);
            return model;
        }

        public static ProdutoView ToView(this Produto model)
        {
            var view = new ProdutoView
            {
                Categoria = model.Categoria,
                Foto = model.Foto,
                Nome = model.Nome,
                Preco = model.Preco,
                Subcategoria = model.Subcategoria,
                Tamanho = model.Tamanho,
                Quantidade = model.Quantidade,
                EstabelecimentoId = model.EstabelecimentoId,
                Estabelecimento = model.Estabelecimento?.ToView()
            };

            GenericViewExtension.ToView(model, view);
            return view;
        }

        public static IQueryable<T> AplicaFiltro<T>(this IQueryable<T> query, ProdutoFiltro filtro)
            where T : Produto
        {
            if (filtro != null)
            {
                if (!string.IsNullOrWhiteSpace(filtro.Categoria))
                    query = query.Where(x => x.Categoria.Equals(filtro.Categoria));

                if (!string.IsNullOrWhiteSpace(filtro.Subcategoria))
                    query = query.Where(x => x.Subcategoria.Equals(filtro.Subcategoria));

                if (filtro.EstabelecimentoId.HasValue)
                    query = query.Where(x => x.EstabelecimentoId == filtro.EstabelecimentoId);

                query = query.AplicaGenericFilter(filtro);
            }

            return query;
        }
    }
}