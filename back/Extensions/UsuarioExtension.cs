using System.Linq;
using Microsoft.EntityFrameworkCore;
using FeiraON.Context.Models;
using FeiraON.Infra.Filters;
using FeiraON.Infra.Views.Models;

namespace FeiraON.Extensions
{
    public static class UsuarioExtension
    {
        public static Usuario ToModel(this UsuarioView view) =>
            new Usuario
            {
                Id = view.Id,
                Ativo = view.Ativo,
                DataCriacao = view.DataCriacao,
                DataModificacao = view.DataModificacao,
                Nome = view.Nome,
                CPF = view.CPF,
                UserName = view.Login,
                PasswordHash = view.Senha,
                Email = view.Email,
                Endereco = view.Endereco,
                RG = view.RG,
                PhoneNumber = view.Telefone,
                Perfil = view.Perfil,
                EstabelecimentoId = view.EstabelecimentoId
            };

        public static UsuarioView ToView(this Usuario model) =>
            new UsuarioView
            {
                Id = model.Id,
                Ativo = model.Ativo,
                DataCriacao = model.DataCriacao,
                DataModificacao = model.DataModificacao,
                Nome = model.Nome,
                CPF = model.CPF,
                Login = model.UserName,
                Email = model.Email,
                Endereco = model.Endereco,
                RG = model.RG,
                Telefone = model.PhoneNumber,
                Perfil = model.Perfil,
                EstabelecimentoId = model.EstabelecimentoId,
                Estabelecimento = model.Estabelecimento?.ToView()
            };

        public static IQueryable<Usuario> AplicaFiltro(this IQueryable<Usuario> query, UsuarioFiltro filtro)
        {
            if (filtro != null)
            {
                if (!string.IsNullOrWhiteSpace(filtro.Login))
                    query = query.Where(x => x.UserName.Equals(filtro.Login));

                if (!string.IsNullOrWhiteSpace(filtro.Telefone))
                    query = query.Where(x => x.PhoneNumber.Contains(filtro.Telefone));

                if (!string.IsNullOrWhiteSpace(filtro.Nome))
                    query = query.Where(x => x.Nome.Contains(filtro.Nome));

                if (filtro.Includes != null && filtro.Includes.Count() > 0)
                    foreach (string include in filtro.Includes)
                        query = query.Include(include);

                if (filtro.Ativo.HasValue)
                    query = query.Where(q => q.Ativo == filtro.Ativo);
            }

            return query;
        }
    }
}