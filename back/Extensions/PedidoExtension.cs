using System.Linq;
using FeiraON.Context.Models;
using FeiraON.Infra.Filters;
using FeiraON.Infra.Views.Models;

namespace FeiraON.Extensions
{
    public static class PedidoExtension
    {
        public static Pedido ToModel(this PedidoView view)
        {
            var model = new Pedido
            {
                Data = view.Data,
                Endereco = view.Endereco,
                Status = view.Status,
                Avaliacao = view.Avaliacao,
                Comentario = view.Comentario,
                UsuarioId = view.UsuarioId,
                EstabelecimentoId = view.EstabelecimentoId
            };

            model.Produtos = view.Produtos?.Select(x =>
                    new ProdutoPedido
                    {
                        Quantidade = x.Quantidade,
                        ProdutoId = x.ProdutoId,
                        Pedido = model
                    }
                ).ToList();

            GenericViewExtension.ToModel(view, model);
            return model;
        }

        public static PedidoView ToView(this Pedido model)
        {
            var view = new PedidoView
            {
                Data = model.Data,
                Endereco = model.Endereco,
                Status = model.Status,
                Avaliacao = model.Avaliacao,
                Comentario = model.Comentario,
                UsuarioId = model.UsuarioId,
                Usuario = model.Usuario?.ToView(),
                EstabelecimentoId = model.EstabelecimentoId,
                Estabelecimento = model.Estabelecimento?.ToView(),
                Produtos = model.Produtos?.Select(x =>
                    new ProdutoPedidoView
                    {
                        Quantidade = x.Quantidade,
                        ProdutoId = x.ProdutoId,
                        Produto = x.Produto?.ToView(),
                        PedidoId = x.PedidoId,
                        // Pedido = x.Pedido?.ToView()
                    }
                )
            };

            GenericViewExtension.ToView(model, view);
            return view;
        }

        public static IQueryable<T> AplicaFiltro<T>(this IQueryable<T> query, PedidoFiltro filtro)
            where T : Pedido
        {
            if (filtro != null)
            {
                if (filtro.Data.HasValue)
                    query = query.Where(x => x.Data.Date.CompareTo(filtro.Data.Value.Date) == 0);

                if (!string.IsNullOrWhiteSpace(filtro.Nome))
                    query = query.Where(x => x.Usuario.Nome.Contains(filtro.Nome));

                if (!string.IsNullOrWhiteSpace(filtro.CPF))
                    query = query.Where(x => x.Usuario.CPF.Contains(filtro.CPF));

                if (filtro.Status.HasValue)
                    query = query.Where(x => x.Status == filtro.Status);

                if (!string.IsNullOrWhiteSpace(filtro.UsuarioId))
                    query = query.Where(x => x.UsuarioId.Equals(filtro.UsuarioId));

                if (filtro.EstabelecimentoId.HasValue)
                    query = query.Where(x => x.EstabelecimentoId == filtro.EstabelecimentoId);

                query = query.AplicaGenericFilter(filtro);
            }

            return query;
        }
    }
}