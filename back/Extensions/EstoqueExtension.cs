using System.Linq;
using FeiraON.Context.Models;
using FeiraON.Infra.Filters;
using FeiraON.Infra.Views.Models;

namespace FeiraON.Extensions
{
    public static class EstoqueExtension
    {
        public static Estoque ToModel(this EstoqueView view)
        {
            var model = new Estoque
            {
                Quantidade = view.Quantidade,
                ProdutoId = view.ProdutoId,
                EstabelecimentoId = view.EstabelecimentoId
            };

            GenericViewExtension.ToModel(view, model);
            return model;
        }

        public static EstoqueView ToView(this Estoque model)
        {
            var view = new EstoqueView
            {
                Quantidade = model.Quantidade,
                ProdutoId = model.ProdutoId,
                Produto = model.Produto?.ToView(),
                EstabelecimentoId = model.EstabelecimentoId,
                Estabelecimento = model.Estabelecimento?.ToView()
            };

            GenericViewExtension.ToView(model, view);
            return view;
        }

        public static IQueryable<T> AplicaFiltro<T>(this IQueryable<T> query, EstoqueFiltro filtro)
            where T : Estoque
        {
            if (filtro != null)
            {
                if (!string.IsNullOrWhiteSpace(filtro.Categoria))
                    query = query.Where(x => x.Produto.Categoria.Contains(filtro.Categoria));

                if (!string.IsNullOrWhiteSpace(filtro.Subcategoria))
                    query = query.Where(x => x.Produto.Subcategoria.Contains(filtro.Subcategoria));

                if (filtro.EstabelecimentoId.HasValue)
                    query = query.Where(x => x.EstabelecimentoId == filtro.EstabelecimentoId);

                query = query.AplicaGenericFilter(filtro);
            }

            return query;
        }
    }
}