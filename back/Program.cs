using System.IO;
using System.Linq;
using System.Reflection;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;

namespace FeiraON
{
    public class Program
    {
        static void Main() =>
            CreateHostBuilder().Build().Run();

        static IHostBuilder CreateHostBuilder()
        {
            var assemblyName = typeof(Startup).GetTypeInfo().Assembly.FullName;

            IHostEnvironment hostingEnvironment = null;

            return Host.CreateDefaultBuilder()
                .UseContentRoot(Directory.GetCurrentDirectory())
                .ConfigureServices(services =>
                {
                    hostingEnvironment = services
                        .Where(x => x.ServiceType == typeof(IHostEnvironment))
                        .Select(x => (IHostEnvironment)x.ImplementationInstance)
                        .First();
                })
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseKestrel(options =>
                    {
                        var config = new ConfigurationBuilder()
                            .SetBasePath(Directory.GetCurrentDirectory())
                            .AddJsonFile($"appsettings.{hostingEnvironment.EnvironmentName}.json", optional: false)
                            .Build();
                    })
                    .UseStartup(assemblyName);
                });
        }
    }
}
